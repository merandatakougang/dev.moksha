import React from "react"
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faAlignJustify,
  faChartBar,
  faCogs,
  faDollarSign, faFile, faFolder,
  faHome, faList, faShippingFast,
  faShoppingCart, faTachometerAlt
} from '@fortawesome/free-solid-svg-icons'
import {faCopy, faMoneyBillAlt} from "@fortawesome/free-regular-svg-icons";

const navigationConfig = [
  {
    id: "dash",
    title: "Dashboard",
    type: "collapse",
    icon: <FontAwesomeIcon icon={faTachometerAlt} size={14}/>,
    badge: "warning",
    children: [
      {
        id: "analyticsDash",
        title: "Ongoing operations",
        type: "item",
        permissions: ["admin", "editor"],
        navLink: "/"
      },
      {
        id: "eCommerceDash",
        title: "Performance indicators",
        type: "item",
        permissions: ["admin"],
        navLink: "/ecommerce-dashboard"
      },
      {
        id: "Sourcingworkflow",
        title: "Sourcing workflow",
        type: "item",
        permissions: ["admin"],
        navLink: ""
      }
    ]
  },
  {
    id: "quotation",
    title: "Quotation",
    type: "item",
    icon: <FontAwesomeIcon icon={faDollarSign} size={14}/>,
    badge: "warning",
    navLink: "/quotations"
  },
  {
    id: "Workorders",
    title: "Work orders",
    type: "item",
    icon: <FontAwesomeIcon icon={faShoppingCart} size={14}/>,
    badge: "warning",
    navLink: "/work-orders"
  },
  {
    id: "Invoices",
    title: "Invoices",
    type: "item",
    icon: <FontAwesomeIcon icon={faFile} size={14}/>,
    badge: "warning",
    navLink: "/invoices"
  },
  {
    id: "Job files",
    title: "Job files",
    type: "item",
    icon: <FontAwesomeIcon icon={faFolder} size={14}/>,
    permissions: ["admin", "editor"],
    navLink: "/job-files"
  },
  {
    id: "Documents",
    title: "Documents",
    type: "item",
    icon: <FontAwesomeIcon icon={faCopy} size={14}/>,
    badge: "warning",
    navLink: "/documents"
  },
  {
    id: "Statistics",
    title: "Statistics",
    type: "collapse",
    icon: <FontAwesomeIcon icon={faChartBar} size={14}/>,
    badge: "warning",
  },
  {
    id: "HScodes",
    title: "HS codes",
    type: "collapse",
    icon: <FontAwesomeIcon icon={faMoneyBillAlt} size={14}/>,
    badge: "warning",
  },
  {
    id: "Customsduties",
    title: "Customs duties",
    type: "collapse",
    icon: <FontAwesomeIcon icon={faList} size={14}/>,
    badge: "warning",
  },
  {
    id: "Accountadministration",
    title: "Account administration",
    type: "collapse",
    icon: <FontAwesomeIcon icon={faCogs}/>,
    badge: "warning",
    children: [
      {
        id: "Myprofile",
        title: "My profile",
        type: "item",
        permissions: ["admin", "editor"],
        navLink: "/"
      },
      {
        id: "Manageusers",
        title: "Manage users",
        type: "item",
        permissions: ["admin"],
        navLink: ""
      }
      ,
      {
        id: "Mycompanyinformation",
        title: "My company information",
        type: "item",
        permissions: ["admin"],
        navLink: ""
      }
    ]
  },
];

export default navigationConfig

