import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";

const CustumBackdrop = withStyles({
  root: {
    backgroundColor: 'rgba(255,255,255,.5)',
  },
})((props) => (
  <Backdrop open={props.isOpen} style={{zIndex: '1000'}} className='position-absolute rounded' {...props}>
    <CircularProgress color="inherit"/>
  </Backdrop>));

export default CustumBackdrop
