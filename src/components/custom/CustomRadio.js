import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import {green} from '@material-ui/core/colors';
import Radio from '@material-ui/core/Radio';

const GreenRadio = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

export default function RadioButtons(props) {
  const [selectedValue, setSelectedValue] = React.useState(props.value);

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };

  return (
    <div className='d-flex align-items-center'>
      <div className='col-10 p-0'>{props.text}</div>
      <div className='d-flex col-1 align-items-center'>
        <Radio
          className='px-0'
          checked={!props.value}
          onChange={()=>{
            props.setValue(false)
          }}
          value="a"
          name="radio-button-demo"
          inputProps={{'aria-label': 'A'}}
        />
        <span>&nbsp;&nbsp;No</span>
      </div>
      <div className='d-flex col-1 align-items-center ml-1'>
        <GreenRadio
          className='px-0'
          checked={props.value}
          onChange={()=>{
            props.setValue(true)
          }}
          value="c"
          name="radio-button-demo"
          inputProps={{'aria-label': 'C'}}
        />
        <span>&nbsp;&nbsp;Yes</span>
      </div>
    </div>
  );
}
