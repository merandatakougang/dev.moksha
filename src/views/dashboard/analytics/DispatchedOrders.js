import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  Table,
  UncontrolledTooltip,
} from "reactstrap"
import avatar1 from "../../../assets/img/portrait/small/avatar-s-5.jpg"
import avatar2 from "../../../assets/img/portrait/small/avatar-s-7.jpg"
import avatar3 from "../../../assets/img/portrait/small/avatar-s-10.jpg"
import avatar4 from "../../../assets/img/portrait/small/avatar-s-8.jpg"

class DispatchedOrders extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Activités sur les 0 dossiers en cours </CardTitle>
        </CardHeader>
        <Table
          responsive
          className="dashboard-table table-hover-animation mb-0 mt-1"
        >
          <thead>
            <tr>
              <th>ORDER</th>
              <th>STATUS</th>
              <th>OPERATORS</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>#879985</td>
              <td>
                <div
                  className="bg-success"
                  style={{
                    height: "10px",
                    width: "10px",
                    borderRadius: "50%",
                    display: "inline-block",
                    marginRight: "5px"
                  }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                      src={avatar1}
                      alt="avatar"
                      height="30"
                      width="30"
                      id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                      src={avatar2}
                      alt="avatar"
                      height="30"
                      width="30"
                      id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                      src={avatar3}
                      alt="avatar"
                      height="30"
                      width="30"
                      id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                      src={avatar4}
                      alt="avatar"
                      height="30"
                      width="30"
                      id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>#879985</td>
              <td>
                <div
                    className="bg-success"
                    style={{
                      height: "10px",
                      width: "10px",
                      borderRadius: "50%",
                      display: "inline-block",
                      marginRight: "5px"
                    }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                        src={avatar1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>#879985</td>
              <td>
                <div
                    className="bg-success"
                    style={{
                      height: "10px",
                      width: "10px",
                      borderRadius: "50%",
                      display: "inline-block",
                      marginRight: "5px"
                    }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                        src={avatar1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>#879985</td>
              <td>
                <div
                    className="bg-success"
                    style={{
                      height: "10px",
                      width: "10px",
                      borderRadius: "50%",
                      display: "inline-block",
                      marginRight: "5px"
                    }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                        src={avatar1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>#879985</td>
              <td>
                <div
                    className="bg-success"
                    style={{
                      height: "10px",
                      width: "10px",
                      borderRadius: "50%",
                      display: "inline-block",
                      marginRight: "5px"
                    }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                        src={avatar1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
            <tr>
              <td>#879985</td>
              <td>
                <div
                    className="bg-success"
                    style={{
                      height: "10px",
                      width: "10px",
                      borderRadius: "50%",
                      display: "inline-block",
                      marginRight: "5px"
                    }}
                />
                <span>Moving</span>
              </td>
              <td className="p-1">
                <ul className="list-unstyled users-list m-0 d-flex">
                  <li className="avatar pull-up">
                    <img
                        src={avatar1}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar1"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar1">
                      Vinnie Mostowy
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar2}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar2"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar2">
                      Elicia Rieske
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar3}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar3"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar3">
                      Julee Rossignol
                    </UncontrolledTooltip>
                  </li>
                  <li className="avatar pull-up">
                    <img
                        src={avatar4}
                        alt="avatar"
                        height="30"
                        width="30"
                        id="avatar4"
                    />
                    <UncontrolledTooltip placement="bottom" target="avatar4">
                      Darcey Nooner
                    </UncontrolledTooltip>
                  </li>
                </ul>
              </td>
            </tr>
          </tbody>
        </Table>
      </Card>
    )
  }
}
export default DispatchedOrders
