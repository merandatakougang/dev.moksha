import React from "react"
import {Row, Col} from "reactstrap"
import "../../../assets/scss/plugins/charts/apex-charts.scss"
import DispatchedOrders from "./DispatchedOrders"
import "../../../assets/scss/pages/dashboard-analytics.scss"
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import {Cpu, DollarSign, Heart, Truck} from "react-feather";
import CurrentOperationcard from './CurrentOperationCard'
import FileCard from './FileCard'

class AnalyticsDashboard extends React.Component {

  componentDidMount() {
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create("chardiv", am4charts.XYChart);

// Add data
    chart.data = [{
      "year": "2003",
      "europe": 25,
      "namerica": 2.5,
      "asia": 2.1,
      "lamerica": 1.2,
      "meast": 0.2,
      "africa": 0.1
    }, {
      "year": "2004",
      "europe": 2.6,
      "namerica": 2.7,
      "asia": 2.2,
      "lamerica": 1.3,
      "meast": 0.3,
      "africa": 0.1
    }, {
      "year": "2005",
      "europe": 2.8,
      "namerica": 2.9,
      "asia": 2.4,
      "lamerica": 1.4,
      "meast": 0.3,
      "africa": 0.1
    }];

// Create axes
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.title.text = "Local country offices";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    categoryAxis.renderer.cellStartLocation = 0.1;
    categoryAxis.renderer.cellEndLocation = 0.9;

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
// valueAxis.title.text = "Expenditure (M)";

// Create series
    function createSeries(field, name, stacked) {
      let series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = field;
      series.dataFields.categoryX = "year";
      series.name = name;
      series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
      series.stacked = stacked;
      series.columns.template.width = am4core.percent(95);
    }

    createSeries("europe", "Europe", false);
    createSeries("namerica", "North America", true);
    createSeries("asia", "Asia", false);
    createSeries("lamerica", "Latin America", true);
    createSeries("meast", "Middle East", true);
    createSeries("africa", "Africa", true);

// Add legend
    chart.legend = new am4charts.Legend();
  }

  render() {
    return (
      <React.Fragment>
        <Row className="d-flex p-0 mb-4">
          <div className="col-2 d-flex align-items-center justify-content-center p-0">
            lorem ipsum
          </div>
          <div className="d-flex col-10">
            <Col lg='3' sm='6'>
              <CurrentOperationcard
                hideChart
                iconRight
                iconBg="background-transparent"
                icon={<Cpu size={22}/>}
                statTitle="CPU Usage"
                backgroundColor="bg-blue"
                textcolor="text-white"
              />
            </Col>
            <Col lg='3' sm='6'>
              <CurrentOperationcard
                hideChart
                iconRight
                iconBg="background-transparent"
                icon={<Cpu size={22}/>}
                stat="86%"
                statTitle="CPU Usage"
                backgroundColor="bg-grey"
                textcolor="text-white"
              />
            </Col>
            <Col lg='3' sm='6'>
              <CurrentOperationcard
                hideChart
                iconRight
                iconBg="background-transparent"
                icon={<Cpu className="" size={22}/>}
                stat="86%"
                statTitle="CPU Usage"
                backgroundColor="bg-green"
                textcolor="text-white"
              />
            </Col>
            <Col lg='3' sm='6'>
              <CurrentOperationcard
                hideChart
                iconRight
                iconBg="background-transparent"
                icon={<Cpu size={22}/>}
                stat="86%"
                statTitle="CPU Usage"
                backgroundColor="bg-blue"
                textcolor="text-white"
              />
            </Col>
          </div>
        </Row>
        <Row>
          <Col sm="7">
            <Row className="mb-4">
              <Col xl="4" lg="4" sm="6">
                <FileCard
                  hideChart
                  iconBg="bg-green"
                  icon={<Truck size={22}/>}
                  stat="2"
                  statTitle="Returns"
                />
              </Col>
              <Col xl="4" lg="4" sm="6">
                <FileCard
                  hideChart
                  iconBg="bg-blue"
                  icon={<DollarSign className="" size={22}/>}
                  stat="3"
                  statTitle="Returns"
                />
              </Col>
              <Col xl="4" lg="4" sm="6">
                <FileCard
                  hideChart
                  iconBg="bg-blue"
                  icon={<Heart className="" size={22}/>}
                  stat="2"
                  statTitle="Returns"
                />
              </Col>
            </Row>
            <div id="chardiv" style={{height: '500px'}}/>
          </Col>
          <Col sm="5">
            <DispatchedOrders/>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default AnalyticsDashboard
