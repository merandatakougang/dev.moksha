import React from "react"
import { Card, CardBody } from "reactstrap"
import Chart from "react-apexcharts"

export default class FileCard extends React.Component {
  render() {
    return (
      <div style={{ background: this.props.backgroundColor }}
        className={`${this.props.backgroundColor ? this.props.backgroundColor : '#ffffff'} rounded box-shadow-1 ${this.props.height ? this.props.height : ''}`}>
        <CardBody
          className="py-4 rounded">
          <div>
            <div className="d-flex align-items-center">
              <h4 className="text-bold-600 col-10 p-0 m-0">8600 $</h4>
              <div className="avatar-content w-100 h-100 mr-2">{this.props.icon}</div>
            </div>
            <p className="mt-2">Lorem ipsum.</p>
          </div>
        </CardBody>
        {!this.props.hideChart && (
          <Chart
            options={this.props.options}
            series={this.props.series}
            type={this.props.type}
            height={this.props.height ? this.props.height : 100}
          />
        )}
      </div>
    )
  }
}
