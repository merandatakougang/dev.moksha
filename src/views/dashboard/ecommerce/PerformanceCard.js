import React from "react"
import { Card, CardBody } from "reactstrap"
import Chart from "react-apexcharts"

export default class PerformanceCard extends React.Component {
  render() {
    return (
      <Card style={{ background: `linear-gradient(45deg,${this.props.backgroundColor},#fafafa)` }}>
        <CardBody
          className="py-4 rounded">
          <div>
            <div className="d-flex">
              <h4 className="text-bold-600 mt-1 mb-25 col-10 p-0 font-weight-bold">8600 $</h4>
              <div className="icon-section col-2 text-right">
                <div className="avatar-content  mr-5">{this.props.icon}</div>
              </div>
            </div>
            <p className="mt-2">Lorem ipsum dolor.</p>
          </div>
        </CardBody>
        {!this.props.hideChart && (
          <Chart
            options={this.props.options}
            series={this.props.series}
            type={this.props.type}
            height={this.props.height ? this.props.height : 100}
          />
        )}
      </Card>
    )
  }
}
