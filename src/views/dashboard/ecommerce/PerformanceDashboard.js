import React from "react"
import {Row, Col} from "reactstrap"
import "../../../assets/scss/plugins/charts/apex-charts.scss"
import {Cpu} from "react-feather";
import PerformanceCard from "./PerformanceCard";

let primary = ["rgb(129 200 247)","rgb(252 168 151)","rgb(181 195 219)","rgb(142 236 141)", "#9c8cfc","#FFC085","#f29292","#b9c3cd","#e7eef7"]

class PerformanceDashboard extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
          {[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0].map((elt,i) => {
            return (
              <Col lg="3" sm="6">
                <PerformanceCard
                  hideChart
                  iconRight
                  iconBg="background-transparent"
                  icon={<Cpu size={22}/>}
                  stat="86%"
                  className="py-5"
                  statTitle="CPU Usage"
                  backgroundColor= {primary[i%4]}
                />
              </Col>
            )
          })}
        </Row>

      </React.Fragment>
    )
  }
}

export default PerformanceDashboard
