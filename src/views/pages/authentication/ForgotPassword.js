import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Col,
  Form,
  Button,
} from "reactstrap"
import logo_itrade_complet_2 from "../../../assets/img/logo/itrade_logo_complet_2.png"
import {history} from "../../../history"
import "../../../assets/scss/pages/authentication.scss"
import TextField from "@material-ui/core/TextField";

class ForgotPassword extends React.Component {
  render() {
    return (
      <Row className="m-0 justify-content-end pr-xl-5">
        <Col
          sm="8"
          xl="5"
          lg="10"
          md="8"
          className="d-flex justify-content-center mr-xl-5"
        >
          <Card className="bg-transparent bg-authentication rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col md="12" className="p-0">
                <Card className="bg-transparent rounded-0 mb-0 justify-content-end">
                  <div className="d-flex justify-content-center">
                    <img src={logo_itrade_complet_2} className="col-5 mt-2" alt="logo_itrade_complet_2"/>
                  </div>
                  <div className='bg-white px-2 pt-1 pb-5 rounded-bottom'>
                    <CardHeader className="pb-1">
                      <CardTitle>
                        <h4 className="mb-0">RECOVER PASSWORD</h4>
                      </CardTitle>
                    </CardHeader>
                    <p className="px-4 auth-title mb-3 pt-2 pb-0">
                      Please enter your email address and we'll send you instructions on how to reset your password.
                    </p>
                    <CardBody className="py-0">
                      <Form>
                        <TextField fullWidth id="standard-basic" label="Email" className="mb-5" />
                        <div className="float-md-left d-block mb-1">
                          <Button.Ripple
                            className="px-3 py-2 btn-block"
                            color="primary"
                            outline
                            onClick={() => history.push("/login")}
                          >
                            Back to Login
                          </Button.Ripple>
                        </div>
                        <div className="float-md-right d-block mb-1">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="px-3 py-2 btn-block"
                            onClick={e => {
                              e.preventDefault()
                              history.push("/")
                            }}
                          >
                            Recover Password
                          </Button.Ripple>
                        </div>
                      </Form>
                    </CardBody>
                  </div>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default ForgotPassword
