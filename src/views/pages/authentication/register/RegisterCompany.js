import React from "react"
import {Form, FormGroup, Input, Label, Button} from "reactstrap"
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"
import {Check} from "react-feather"
import {connect} from "react-redux"
import {signupWithFirebase} from "../../../../redux/actions/auth/registerActions"
import {history} from "../../../../history"
import RegisterIndividualStep0 from "./RegistrationSteps/RegistrationStep0";
import RegisterIndividualStep1 from "./RegistrationSteps/RegistrationStep1";
import RegisterIndividualStep2 from "./RegistrationSteps/RegistrationStep2";

class RegisterCompany extends React.Component {
  state = {
    civility: "",
    email: "",
    countryId: "",
    firstName: "",
    city: "",
    lastName: "",
    step: 0,
    canRegister: false,
    address: "",
    functionInCompany: "",
    password: "",
    confirmPass: "",
    client: {
      typeOfClient: "company",
      companyName: "bychrisme",
      companyAdress: "Logbessou",
      phoneNumber: "237693532571"
    },
  }

  handleRegister = e => {
    e.preventDefault()
    this.props.signupWithFirebase(
      this.state.email,
      this.state.password,
      this.state.name
    )
  }

  render() {
    return (
      <Form onSubmit={this.handleRegister}>
        <div>
          {(() => {
            switch (this.state.step) {
              case 0 :
                return <RegisterIndividualStep0 state={this.state}/>;
              case 1 :
                return <RegisterIndividualStep1 state={this.state}/>;
              case 2 :
                return <RegisterIndividualStep2 state={this.state}/>;

            }
          })()}
        </div>
        <div className="d-flex justify-content-between">
          <Button.Ripple
            className="px-3 py-2"
            color="primary"
            outline
            onClick={() => {
              history.push("/login")
            }}
          >
            Login
          </Button.Ripple>
          <div className="justify-content-end">
            {
              this.state.step > 0 ? <Button.Ripple
                className="px-3 py-2 mr-1"
                color="primary"
                outline
                onClick={() => {
                  if (this.state.step > 0)
                    this.setState({step: this.state.step - 1})
                }}
              >
                Previous
              </Button.Ripple> : () => {
              }
            }
            <Button.Ripple
              className="px-3 py-2"
              color="primary"
              type="submit"
              onClick={() => {
                if (this.state.step < 2)
                  this.setState({step: this.state.step + 1})
              }}>
              {this.state.step < 2 ? "Next" : "Register"}
            </Button.Ripple>
          </div>
        </div>

      </Form>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
}
export default connect(mapStateToProps, {signupWithFirebase})(
  RegisterCompany
)
