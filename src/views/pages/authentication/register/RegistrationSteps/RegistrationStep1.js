import {FormGroup, Input, Label} from "reactstrap";
import Checkbox from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import {Check} from "react-feather";
import React from "react";
import TextField from "@material-ui/core/TextField";

export default class RegisterIndividualStep1 extends React.Component {
  state = this.props.state

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="d-flex">
          <TextField
            type="text"
            label="Civility"
            required
            className="mb-3 pl-0 ml-0 col-3 pr-0 mr-2"
            fullWidth
            value={this.state.civility}
            onChange={e => this.setState({civility: e.target.value})}/>
          <TextField
            type="text"
            label="First name"
            required
            className="mb-3 pl-0 ml-0 col-9 pr-0 pr-2"
            fullWidth
            value={this.state.firstName}
            onChange={e => this.setState({firstName: e.target.value})}/>
        </div>
        <TextField
          type="text"
          label="Surname"
          required
          className="mb-3 pl-0 ml-0  pr-0 mr-2"
          fullWidth
          value={this.state.lastName}
          onChange={e => this.setState({lastName: e.target.value})}/>
        <TextField
          type="email"
          label="Email"
          required
          className="mb-3 pl-0 ml-0 pr-0"
          fullWidth
          value={this.state.email}
          onChange={e => this.setState({email: e.target.value})}/>
        <div className="d-flex">
        <TextField
          type="text"
          label="Country"
          required
          className="mb-3 pl-0 ml-0  pr-0 mr-2 col-6"
          fullWidth
          value={this.state.countryId}
          onChange={e => this.setState({countryId: e.target.value})}/>
        <TextField
          type="text"
          label="City"
          required
          className="mb-5 pl-0 ml-0 pr-0 col-6"
          fullWidth
          value={this.state.city}
          onChange={e => this.setState({city: e.target.value})}/>
        </div>
        <FormGroup>
          <Checkbox
            color="primary"
            icon={<Check className="vx-icon" size={16}/>}
            label=" I accept the terms & conditions."
            defaultChecked={true}
          />
        </FormGroup>
      </div>)
  }
}
