import {FormGroup, Input, Label} from "reactstrap";
import Checkbox from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import {Check} from "react-feather";
import React from "react";
import TextField from "@material-ui/core/TextField";

export default class RegisterIndividualStep2 extends React.Component {

  state = this.props.state

  constructor(props) {
    super(props);
  }


  render() {
    return (
      <div>
        <TextField
          type="text"
          label="Name"
          required
          className="mb-3 pl-0 mx-0  pr-0"
          fullWidth
          value={this.state.client.phoneNumber}
          onChange={e => this.setState({client:{name: e.target.value}})}/>
        <TextField
          type="email"
          label="Email"
          required
          className="mb-3 pl-0 mx-0  pr-0"
          fullWidth
          value={this.state.address}
          onChange={e => this.setState({email: e.target.value})}/>
        <TextField
          type="password"
          label="Password"
          required
          className="mb-3 pl-0 mx-0  pr-0"
          fullWidth
          value={this.state.password}
          onChange={e => this.setState({password: e.target.value})}/>
        <TextField
          type="password"
          label="Confirm Password"
          required
          className="mb-5 pl-0 mx-0  pr-0"
          fullWidth
          value={this.state.confirmPass}
          onChange={e => this.setState({confirmPass: e.target.value})}/>
        <FormGroup>
          <Checkbox
            color="primary"
            icon={<Check className="vx-icon" size={16}/>}
            label=" I accept the terms & conditions."
            defaultChecked={true}
          />
        </FormGroup>
      </div>)
  }
}
