import {FormGroup, Input, Label} from "reactstrap";
import Checkbox from "../../../../../components/@vuexy/checkbox/CheckboxesVuexy";
import {Check} from "react-feather";
import React from "react";
import TextField from "@material-ui/core/TextField";

export default class RegisterIndividualStep1 extends React.Component {
  state = this.props.state

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <TextField
          type="text"
          label="Company name"
          required
          className="mb-4"
          fullWidth
          value={this.state.client.companyName}
          onChange={e => this.setState({client: {companyName: e.target.value}})}
        />
        <TextField
          type="text"
          label="Address"
          required
          className="mb-5"
          fullWidth
          value={this.state.client.companyAdress}
          onChange={e => this.setState({client: {companyAdress: e.target.value}})}
        />
        <FormGroup>
          <Checkbox
            color="primary"
            icon={<Check className="vx-icon" size={16}/>}
            label=" I accept the terms & conditions."
            defaultChecked={true}
          />
        </FormGroup>
      </div>)
  }
}
