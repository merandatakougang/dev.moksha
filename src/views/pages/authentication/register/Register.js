import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Row,
  Col,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from "reactstrap"
import classnames from "classnames"
import RegisterCompany from "./RegisterCompany"
import RegisterIndividual from "./RegisterIndividual"
import logo_itrade_complet_2 from "../../../../assets/img/logo/itrade_logo_complet_2.png"
import "../../../../assets/scss/pages/authentication.scss"

class Register extends React.Component {
  state = {
    activeTab: "1"
  }
  toggle = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  render() {
    return (
      <Row className="pr-md-4 m-0 justify-content-md-end justify-content-sm-center">
        <Col sm="8" xl="4" lg="6" md="6" className="mr-md-5 d-flex justify-content-center">
          <Card className="bg-transparent mr-xl-5 bg-authentication login-card rounded-0 mb-0 w-100">
            <Row className="m-0">
              <Col lg="12" md="12" className="p-0">
                <Card className="bg-transparent rounded-0 mb-0 login-tabs-container">
                  <div className="d-flex justify-content-center">
                    <img src={logo_itrade_complet_2} className="col-10 mt-2" alt="logo_itrade_complet_2"/>
                  </div>
                  <div className="bg-white px-2 pb-2">
                    <CardHeader className="pb-1">
                      <CardTitle>
                        <h4 className="mb-0">CREATE ACCOUNT</h4>
                      </CardTitle>
                    </CardHeader>
                    <p className="px-4 pt-2 pb-3 auth-title mb-0">
                      Fill the below form to create a new account.
                    </p>
                    <Nav tabs className="px-4 pb-1">
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "1"
                          })}
                          onClick={() => {
                            this.toggle("1")
                          }}
                        >
                          INDIVIDUAL
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink
                          className={classnames({
                            active: this.state.activeTab === "2"
                          })}
                          onClick={() => {
                            this.toggle("2")
                          }}
                        >
                          COMPANY
                        </NavLink>
                      </NavItem>
                    </Nav>
                    <CardBody className="pt-1 pb-50">
                      <TabContent activeTab={this.state.activeTab}>
                        <TabPane tabId="1">
                          <RegisterIndividual/>
                        </TabPane>
                        <TabPane tabId="2">
                          <RegisterCompany/>
                        </TabPane>
                      </TabContent>
                    </CardBody>
                  </div>
                </Card>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default Register
