import React from "react"
import {Form,Button} from "reactstrap"
import {connect} from "react-redux"
import {signupWithJWT} from "../../../../redux/actions/auth/registerActions"
import {history} from "../../../../history"
import RegisterIndividualStep1 from "./RegistrationSteps/RegistrationStep1";
import RegisterIndividualStep2 from "./RegistrationSteps/RegistrationStep2";

class RegisterIndividual extends React.Component {
  state = {
    civility: "",
    email: "",
    country: "",
    name: "",
    city: "",
    surname: "",
    step: 1,
    canRegister: false,
    phone_number: "",
    address: "",
    password: "",
    confirmPass: ""
  }

  handleRegister = e => {
    e.preventDefault()
    this.props.signupWithJWT(
      this.state.email,
      this.state.password,
      this.state.name
    )
  }

  render() {
    return (
      <Form action="/" onSubmit={this.handleRegister}>

        <div>
          {this.state.step === 1 ? <RegisterIndividualStep1 state={this.state}/> :
            <RegisterIndividualStep2 state={this.state}/>}
        </div>

        <div className="d-flex justify-content-between">
          <Button.Ripple
            className="px-3 py-2"
            color="primary"
            outline
            onClick={() => {
              history.push("/login")
            }}
          >
            Login
          </Button.Ripple>
          <div className="justify-content-end">
            {
              this.state.step > 1 ? <Button.Ripple
                className="px-3 py-2 mr-1"
                color="primary"
                outline
                onClick={() => {
                  if (this.state.step > 1)
                  this.setState({step: this.state.step - 1})
                }}
              >
                Previous
              </Button.Ripple> : () => {
              }
            }
            <Button.Ripple
              className="px-3 py-2"
              color="primary"
              type="submit"
              onClick={() => {
                if (this.state.step < 2)
                  this.setState({step: this.state.step + 1})
              }}>
              {this.state.step < 2 ? "Next" : "Register"}
            </Button.Ripple>
          </div>
        </div>
      </Form>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
}

export default connect(mapStateToProps, {signupWithJWT})(RegisterIndividual)
