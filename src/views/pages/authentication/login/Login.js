import React, {useState} from "react"
import {useHistory} from 'react-router-dom'
import {Card, CardHeader, CardTitle, Row, Col} from "reactstrap"
import logo_itrade_complet_2 from "../../../../assets/img/logo/itrade_logo_complet_2.png"
import "../../../../assets/scss/pages/authentication.scss"
import LoginBody from "./LoginBody"
import AuthService from "../../../../services/authService";

export default function Login() {

  const history = useHistory(),
    [loading, setLoading] = useState(false),
    login = (data) => {
      setLoading(true)
      return new AuthService().login(data).then(res => {
        setLoading(false)
        if (res.status === 200) {
          history.push('/')
        } else if (res.status === 204)
          console.log(res)
      }).catch(rej => console.log(rej))
    }
  return (
    <Row className="pr-md-4 m-0 justify-content-md-end justify-content-sm-center">
      <Col sm="8" xl="4" lg="6" md="6" className="mr-md-5 d-flex justify-content-center">
        <Card className="bg-transparent mr-xl-5 bg-authentication login-card rounded-0 mb-0 w-100">
          <Row className="m-0">
            <Col lg="12" md="12" className="p-0">
              <Card className="bg-transparent rounded-0 mb-0 login-tabs-container">
                <div className="d-flex justify-content-center">
                  <img src={logo_itrade_complet_2} className="col-10 mt-2" alt="logo_itrade_complet_2"/>
                </div>
                <div className="bg-white px-2 ">
                  <CardHeader className="pb-1">
                    <CardTitle>
                      <h4 className="mb-0">LOGIN</h4>
                    </CardTitle>
                  </CardHeader>
                  <p className="px-4 pt-3 pb-3 auth-title">
                    Welcome back, please login to your account.
                  </p>
                  <LoginBody login={login} loading={loading}/>
                </div>
              </Card>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  )
}
