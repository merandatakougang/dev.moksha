import React from "react"
import {CardBody, FormGroup, Form, Button, Label} from "reactstrap"
import {Link} from "react-router-dom"
import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"
import {Mail, Lock, Check, Facebook, Twitter, GitHub} from "react-feather"
import {history} from "../../../../history"
import googleSvg from "../../../../assets/img/svg/google.svg"
import {connect} from "react-redux"
import {
  submitLoginWithFireBase,
  loginWithFB,
  loginWithTwitter,
  loginWithGoogle,
  loginWithGithub
} from "../../../../redux/actions/auth/loginActions"
import TextField from "@material-ui/core/TextField";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import CircularProgress from "@material-ui/core/CircularProgress";

class LoginBody extends React.Component {
  state = {
    email: "client01@demo.com",
    password: "client@2020",
    remember: false,
    showPassword: false,
  }

  handleLogin = e => {
    this.props.login({
      email: this.state.email,
      password: this.state.password,
      remember: this.state.remember
    })
  }
  handleClickShowPassword = () => {
    this.setState({showPassword: !this.state.showPassword});
  };

  handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  handleRemember = e => {
    this.setState({
      remember: e.target.checked
    })
  }

  render(props) {
    return (
      <React.Fragment>

        <CardBody className="">
          <Form onSubmit={this.handleLogin}>
            <TextField
              fullWidth
              type="email"
              value={this.state.email}
              onChange={e => this.setState({email: e.target.value})}
              required
              id="standard-basic"
              label="Email"/>
            <FormControl fullWidth className="mt-4 mb-4">
              <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
              <Input
                id="standard-adornment-password"
                type={this.state.showPassword ? 'text' : 'password'}
                value={this.state.password}
                onChange={ev => this.setState({password: ev.target.value})}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      className="mb-3"
                      aria-label="toggle password visibility"
                      onClick={this.handleClickShowPassword}
                      onMouseDown={this.handleMouseDownPassword}
                    >
                      {this.state.showPassword ? <Mail/> : <Mail/>}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
            <FormGroup className="d-flex justify-content-between align-items-center">
              <Checkbox
                color="primary"
                icon={<Check className="vx-icon" size={16}/>}
                label="Remember me"
                defaultChecked={false}
                onChange={this.handleRemember}
              />
              <div className="float-right">
                <Link to="/forgot-password">Forgot Password?</Link>
              </div>
            </FormGroup>
            <div className="d-flex justify-content-between">
              <Button.Ripple
                className="px-3 py-2"
                color="primary"
                outline
                onClick={() => {
                  history.push("/register")
                }}
              >
                Register
              </Button.Ripple>
              <span className='d-flex align-items-center'>
                {this.props.loading ? <CircularProgress className='mr-2'/> : null}
                <Button.Ripple color="primary" className="px-3 py-2" onClick={this.handleLogin}>
                Login
              </Button.Ripple>
              </span>
            </div>
          </Form>
        </CardBody>
        <div className="auth-footer">
          <div className="divider mb-0 mt-0">
            <div className="divider-text">OR</div>
          </div>
          <div className="footer-btn d-flex justify-content-center">
            <Button.Ripple
              className="btn-facebook"
              color=""
              onClick={() => {
                this.props.loginWithFB()
              }}
            >
              <Facebook size={14}/>
            </Button.Ripple>
            <Button.Ripple
              className="btn-twitter"
              color=""
              onClick={this.props.loginWithTwitter}
            >
              <Twitter size={14} stroke="white"/>
            </Button.Ripple>
            <Button.Ripple
              className="btn-google"
              color=""
              onClick={this.props.loginWithGoogle}
            >
              <img src={googleSvg} alt="google" height="15" width="15"/>
            </Button.Ripple>
            <Button.Ripple
              className="btn-github mr-1"
              color=""
              onClick={this.props.loginWithGithub}
            >
              <GitHub size={14} stroke="white"/>
            </Button.Ripple>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
}

export default connect(mapStateToProps, {
  submitLoginWithFireBase,
  loginWithFB,
  loginWithTwitter,
  loginWithGoogle,
  loginWithGithub
})(LoginBody)
