import React from "react"
import Select from "react-select"
import {
  Button,
  Row,
  Col,
  FormGroup,
  Input,
  CardBody,
  Pagination,
  PaginationItem,
  PaginationLink
} from "reactstrap"
import {
  Grid,
  List,
  Search,
  ChevronLeft,
  ChevronRight,
  Menu, Folder
} from "react-feather"
import {data} from "./shopData"
import {Link} from "react-router-dom"
import "../../assets/scss/plugins/forms/react-select/_react-select.scss"
import Badge from "@material-ui/core/Badge";

const sortOptions = [
  {
    value: "featured",
    label: "Featured"
  },
  {
    value: "lowest",
    label: "Lowest"
  },
  {
    value: "highest",
    label: "Highest"
  }
]

class DocumentBody extends React.Component {
  state = {
    inCart: [],
    inWishlist: [],
    view: "grid-view"
  }

  handleAddToCart = i => {
    let cartArr = this.state.inCart
    cartArr.push(i)
    this.setState({
      inCart: cartArr
    })
  }

  handleView = view => {
    this.setState({
      view
    })
  }

  handleWishlist = i => {
    let wishlistArr = this.state.inWishlist
    if (!wishlistArr.includes(i)) wishlistArr.push(i)
    else wishlistArr.splice(wishlistArr.indexOf(i), 1)
    this.setState({
      inWishlist: wishlistArr
    })
  }

  render() {
    let renderProducts = data.map((product, i) => {
      return (
        <div className="col-sm-12 col-md-4 col-lg-3 col-xl-2" key={i}>
          <div className="card-content">
            <div className="item-img text-center d-flex align-items-center justify-content-center">
              <Badge badgeContent={4} color="secondary">
                <Folder className="img-fluid" size={100}/>
              </Badge>
            </div>
            <CardBody>
              <div className="item-wrapper d-flex align-items-center mb-2">
                <div className="col-12 text-center">{product.name}</div>
              </div>
            </CardBody>
          </div>
        </div>
      )
    })
    return (
      <div>
        <Row>
          <Col sm="12">
            <div className="ecommerce-header-items">
              <div className="result-toggler w-25 d-flex align-items-center">
                <div className="shop-sidebar-toggler d-block d-lg-none">
                  <Menu
                    size={26}
                    onClick={() => this.props.mainSidebar(true)}
                  />
                </div>
                <div className="search-results h3">16285 Results Found</div>
              </div>
              <div className="view-options d-flex justify-content-end w-75">
                <Select
                  className="React-Select"
                  classNamePrefix="select"
                  defaultValue={sortOptions[0]}
                  name="sort"
                  options={sortOptions}
                />
                <div className="view-btn-option">
                  <Button
                    color="white"
                    className={`view-btn ml-1 ${
                      this.state.view === "grid-view" ? "active" : ""
                    }`}
                    onClick={() => this.handleView("grid-view")}
                  >
                    <Grid size={24}/>
                  </Button>
                  <Button
                    color="white"
                    className={`view-btn ${
                      this.state.view === "list-view" ? "active" : ""
                    }`}
                    onClick={() => this.handleView("list-view")}
                  >
                    <List size={24}/>
                  </Button>
                </div>
              </div>
            </div>
          </Col>
          <Col sm="12">
            <div className="ecommerce-searchbar mt-4">
              <FormGroup className="position-relative">
                <Input
                  className="search-product"
                  placeholder="Search Here..."
                />
                <div className="form-control-position">
                  <Search size={22}/>
                </div>
              </FormGroup>
            </div>
          </Col>
          <Col sm="12" className="mt-5 pb-4">
            <div className="d-flex flex-wrap">
              {renderProducts}
            </div>
          </Col>
          <Col sm="12">
            <div className="ecommerce-pagination">
              <Pagination className="d-flex justify-content-center mt-2">
                <PaginationItem className="prev-item">
                  <PaginationLink href="#" first>
                    <ChevronLeft/>
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem active>
                  <PaginationLink href="#">1</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">2</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">3</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">4</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">5</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">6</PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">7</PaginationLink>
                </PaginationItem>
                <PaginationItem href="#" className="next-item">
                  <PaginationLink href="#" last>
                    <ChevronRight/>
                  </PaginationLink>
                </PaginationItem>
              </Pagination>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default DocumentBody
