import React from "react"
import DocumentBody from "./document-body"
import "../../assets/scss/pages/app-ecommerce-shop.scss"

const mql = window.matchMedia(`(min-width: 992px)`)
export default class Documents extends React.Component {
  state = {
    sidebarDocked: mql.matches,
    sidebarOpen: false
  }

  UNSAFE_componentWillMount() {
    mql.addListener(this.mediaQueryChanged)
  }

  componentWillUnmount() {
    mql.removeListener(this.mediaQueryChanged)
  }

  onSetSidebarOpen = open => {
    this.setState({sidebarOpen: open})
  }

  mediaQueryChanged = () => {
    this.setState({sidebarDocked: mql.matches, sidebarOpen: false})
  }

  render() {
    return (
      <React.Fragment>
        <div className="ecommerce-application">
          <DocumentBody
            mainSidebar={this.onSetSidebarOpen}
            sidebar={this.state.sidebarOpen}/>
        </div>
      </React.Fragment>
    )
  }
}
