import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {FileText} from "react-feather";
import ListItemText from "@material-ui/core/ListItemText";
import {CardBody, Progress} from "reactstrap";
import {LinearProgress, withStyles} from "@material-ui/core";

const JobFileListMode = props => {
  return (
    <div>
      <List dense={false} className='d-flex col-12 flex-wrap'>
        {props.list.map(elt => {
          return (
            <ListItem className='col-lg-4 col-xl-3 col-sm-12 col-md-6' button>
              <ListItemIcon className='mr-3'>
                <FileText size={54}/>
              </ListItemIcon>
              <ListItemText
                primary="Single-line item"
                secondary={'secondary' ? (
                  <div>
                    <div className='mt-2'>Lorem ipsum dolor sit amet.</div>
                    <div className='mt-2'>Lorem ipsum dolor sit amet.</div>
                    <BorderLinearProgress variant="determinate" className='mt-2' value={20}/>
                  </div>
                ) : null}
              />
              <Progress value={26}/>
            </ListItem>)
        })}
      </List>
    </div>
  )
}

export default JobFileListMode



const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    maxWidth: 156,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

