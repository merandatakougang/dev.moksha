import React, {useState} from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  ButtonGroup,
  Button,
  UncontrolledButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem
} from "reactstrap";
import {
  List as ListSvg,
  Grid as GridSvg,
  Filter,
  Search,
  ChevronDown,
  Trash2,
  Clipboard, Printer, Download
} from "react-feather";
import IconButton from "@material-ui/core/IconButton";
import * as classnames from "classnames";
import JobFileArrayMode from "./job-file-array-mode";
import JobFileListMode from "./job-file-list-mode";

const JobFiles = () => {

  const [list, setList] = useState([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]),
    [viewMode, setViewMode] = useState('list'),
    jobFileType = ['Closed files', 'Ongoing files', 'All files'],
    [jobFileIndex, setJobFileIndex] = useState(2),
    [collapsed, setCollapsed] = useState(false)
  return (
    <div>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <Col sm="12">
                <div className='bg-accent-1 d-flex'>
                  <ButtonGroup className='box-shadow-0'>
                    {jobFileType.map((elt, i) => {
                      return (
                        <Button
                          className={classnames('rounded', 'bg-transparent', 'tabs-options', jobFileIndex === i ? 'bottom-bord' : '')}
                          color='whitesmoke'
                          onClick={() => setJobFileIndex(i)}>{elt}
                        </Button>
                      )
                    })}
                  </ButtonGroup>
                </div>
                <div className='pl-3 mt-3 pb-2'>
                  <div>
                    Get and manage and manage all files lists information related to the quotations and activities done
                    that since your registration.
                  </div>
                </div>
              </Col>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <CardBody>
              <div className='d-flex justify-content-end align-items-center px-5 pt-3 pb-4 '>
                <div
                  className={classnames('rounded', 'mr-2', !collapsed ? '' : 'w-50', 'd-flex', 'justify-content-end', 'box-shadow-1')}>
                  {collapsed ? <input type="text" className='w-100 border-0 pl-5'
                                      placeholder='Which folder are you looking for ?'/> : ''}
                  <IconButton className='rounded ' onClick={() => setCollapsed(!collapsed)}>
                    <Search/>
                  </IconButton>
                </div>
                <IconButton className='mr-2 rounded box-shadow-1'>
                  <Filter/>
                </IconButton>
                <div className='border-3 box-shadow-1 rounded mr-2'>
                  <IconButton onClick={() => setViewMode('array')}>
                    <ListSvg/>
                  </IconButton>
                  <IconButton onClick={() => setViewMode('list')}>
                    <GridSvg/>
                  </IconButton>
                  {/*<IconButton>*/}
                  {/*  <FolderMinus/>*/}
                  {/*</IconButton>*/}
                </div>
                <div className="filter-actions  d-flex align-items-center justify-content-end">
                  <div className="dropdown box-shadow-1 rounded">
                    <UncontrolledButtonDropdown className='d-flex '>
                      <DropdownToggle color="white">
                        Actions
                        <ChevronDown className="ml-50" size={15}/>
                      </DropdownToggle>
                      <DropdownMenu right>
                        <DropdownItem tag="a">
                          <Trash2 size={15}/>&nbsp;&nbsp;
                          <span className="align-middle ml-50">Delete</span>
                        </DropdownItem>
                        <DropdownItem tag="a">
                          <Clipboard size={15}/>&nbsp;&nbsp;
                          <span className="align-middle ml-50">Archive</span>
                        </DropdownItem>
                        <DropdownItem tag="a">
                          <Printer size={15}/>&nbsp;&nbsp;
                          <span className="align-middle ml-50">Print</span>
                        </DropdownItem>
                        <DropdownItem tag="a">
                          <Download size={15}/>&nbsp;&nbsp;
                          <span className="align-middle ml-50">CSV</span>
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </div>
                </div>
              </div>
              {viewMode === 'list' ? <JobFileListMode list={list}/> : <JobFileArrayMode/>}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
  )
}

export default JobFiles
