import React from "react"
import {
  Card,
  CardBody,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap"
import axios from "axios"
import {ContextLayout} from "../../utility/context/Layout"
import {AgGridReact} from "ag-grid-react"
import {
  Edit,
  Trash2,
  ChevronDown,
} from "react-feather"
import {history} from "../../history"
import "../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../assets/scss/pages/users.scss"
import {LinearProgress, withStyles} from "@material-ui/core";

class JobFileArrayMode extends React.Component {

  handleClickOpen = () => {
    const getQuoteOpen = true;
    this.setState({getQuoteOpen});
  };

  handleClickClose = () => {
    const getQuoteOpen = false;
    this.setState({getQuoteOpen});
  };

  state = {
    rowData: null,
    pageSize: 20,
    isVisible: true,
    reload: false,
    isFiltering: false,
    collapse: false,
    getQuoteOpen: false,
    status: "Opened",
    role: "All",
    selectStatus: "All",
    verified: "All",
    department: "All",
    defaultColDef: {
      sortable: true
    },
    quoteIndex: 3,
    quotationType: [
      'Quote to approves',
      'Accepted quotes',
      'Rejected quotes',
      'Expired quotes',
      'All quotes'
    ],
    columnDefs: [
      {
        headerName: "N° Folder",
        field: "id",
        width: 200,
        filter: true,
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true
      },
      {
        headerName: "My Reference",
        field: "username",
        filter: true,
        width: 250,
      },
      {
        headerName: "N° BL",
        field: "email",
        filter: true,
        width: 250
      },
      {
        headerName: "Service",
        field: "name",
        filter: true,
        width: 250
      },
      {
        headerName: "CFR",
        field: "country",
        filter: true,
        width: 250
      },
      {
        headerName: "ETA",
        field: "country",
        filter: true,
        width: 250
      },
      {
        headerName: "Status",
        field: "status",
        filter: true,
        width: 250,
        cellRendererFramework: params => <BorderLinearProgress variant="determinate" className='mt-2' value={50}/>
      },
      {
        headerName: "Actions",
        field: "transactions",
        width: 150,
        cellRendererFramework: () => {
          return (
            <div className="actions cursor-pointer">
              <Edit
                className="mr-75"
                size={16}
                onClick={() => history.push("/app/user/edit")}
              />
              <Trash2
                size={16}
                onClick={() => {
                  let selectedData = this.gridApi.getSelectedRows()
                  this.gridApi.updateRowData({remove: selectedData})
                }}
              />
            </div>
          )
        }
      }
    ]
  }

  async componentDidMount() {
    await axios.get("api/users/list").then(response => {
      let rowData = response.data
      this.setState({rowData})
    })
  }

  filterData = (column, val) => {
    var filter = this.gridApi.getFilterInstance(column)
    var modelObj = null
    if (val !== "all") {
      modelObj = {
        type: "equals",
        filter: val
      }
    }
    filter.setModel(modelObj)
    this.gridApi.onFilterChanged()
  }

  filterSize = val => {
    if (this.gridApi) {
      this.gridApi.paginationSetPageSize(Number(val))
      this.setState({
        pageSize: val
      })
    }
  }
  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }

  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }

  render() {
    const {rowData, columnDefs, defaultColDef, pageSize} = this.state
    return (
      <Row className="app-quotations-list position-relative mt-2">
        <Col sm="12">
          <Card>
            <CardBody>
              <div className="ag-theme-material ag-grid-table pb-5 pt-2" style={{height: "calc(100vh - 100px)"}}>
                <div className="ag-grid-actions d-flex justify-content-between flex-wrap mb-3">
                  <div className='d-flex align-items-center'>
                    <div className="sort-dropdown">
                      <UncontrolledDropdown className="ag-dropdown px-3 py-2 ml-3">
                        <DropdownToggle tag="div">
                          1 - {pageSize} of 150
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="div" onClick={() => this.filterSize(20)}>20</DropdownItem>
                          <DropdownItem tag="div" onClick={() => this.filterSize(50)}>50</DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(100)}
                          >
                            100
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(150)}
                          >
                            150
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>
                  </div>
                </div>
                {this.state.rowData !== null ? (
                  <ContextLayout.Consumer>
                    {context => (
                      <AgGridReact
                        gridOptions={{roupUseEntireRow: true}}
                        rowSelection="multiple"
                        defaultColDef={defaultColDef}
                        columnDefs={columnDefs}
                        rowData={rowData}
                        colResizeDefault={"shift"}
                        animateRows={true}
                        floatingFilter={this.state.isFiltering}
                        pagination={true}
                        pivotPanelShow="always"
                        onSelectionChanged={this.onSelectionChanged}
                        paginationPageSize={pageSize}
                        resizable={true}
                        enableRtl={context.state.direction === "rtl"}
                      />
                    )}
                  </ContextLayout.Consumer>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default JobFileArrayMode


const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    maxWidth: 200,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

