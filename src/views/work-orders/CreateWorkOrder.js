import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CreateWorkOrderBody from "./CreateWorkOrderBody";

const useStyles = makeStyles((theme) => ({
  dialogContainer: {
    height: '100vh'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
}));

const CreateWorkOrder = props => {
  const classes = useStyles(),
    fullWidth = true,
    maxWidth = 'md';

  return (
    <React.Fragment>
      <Dialog
        fullWidth={fullWidth}
        scroll={'paper'}
        maxWidth={maxWidth}
        open={props.open}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title">Optional sizes</DialogTitle>
        <DialogContent className={classes.dialogContainer}>
          <CreateWorkOrderBody/>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={props.setClose}
            color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
export default CreateWorkOrder;
