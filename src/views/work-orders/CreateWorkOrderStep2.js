import React from "react";
import TextField from "@material-ui/core/TextField";

const CreateWorkOrderStep2 = props => {
  return (
    <div className='mt-4'>
      <div className='mb-4 col-10'>
        <TextField
          label="Clearing intructions"
          multiline
          rowsMax={4}
          rows={4}
          fullWidth
          variant="filled"
        />
      </div>
      <div className='mb-4 col-10'>
        <TextField
          label="Billing instructions"
          multiline
          rowsMax={4}
          rows={4}
          fullWidth
          variant="filled"
        />
      </div>
      <div className='mb-4 col-10'>
        <TextField
          label="Address and delivery instructions"
          multiline
          rowsMax={4}
          rows={4}
          fullWidth
          variant="filled"
        />
      </div>
    </div>
  )
}

export default CreateWorkOrderStep2;
