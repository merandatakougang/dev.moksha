import React from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/Input";

const CreateWorkOrderStep1 = props => {

  return (
    <div className='mt-2'>
      <div className='d-flex mb-3 col-10'>
        <div className='col-4'>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Quotation N°*</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select">
              <MenuItem value={10}>Sea</MenuItem>
              <MenuItem value={20}>Air</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className='col-4'>
          <TextField label="My Reference" fullWidth/>
        </div>
        <div className='col-4'>
          <TextField label="Vesssel name" fullWidth/>
        </div>
      </div>
      <div className='d-flex mb-3 col-10'>
        <div className='col-4'>
          <FormControl>
            <InputLabel id="demo-simple-select-label">Weight</InputLabel>
            <Input type='number' endAdornment={<InputAdornment position="end">Kg</InputAdornment>}/>
          </FormControl>
        </div>
        <div className='col-4'>
          <FormControl>
            <InputLabel id="demo-simple-select-label">Volume*</InputLabel>
            <Input type='number' endAdornment={<InputAdornment position="end">Sqm</InputAdornment>}/>
          </FormControl>
        </div>
        <div className='col-4'>
          <TextField label="Number of packages" type='number' fullWidth/>
        </div>
      </div>
      <div className='d-flex mb-3 col-10'>
        <div className='col-4'>
          <TextField label="Booking N°" fullWidth/>
        </div>
        <div className='col-4'>
          <TextField
            label="Departure date"
            type="date"
            defaultValue="2017-05-24"
            InputLabelProps={{shrink: true}}
            fullWidth/>
        </div>
        <div className='col-4'>
          <TextField
            label="Booking date"
            type="date"
            defaultValue="2017-05-24"
            InputLabelProps={{shrink: true}}
            fullWidth/>
        </div>
      </div>
      <div className='d-flex mb-5 col-10'>
        <div className='col-4'>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Customs regime*</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select">
              <MenuItem value={10}>Sea</MenuItem>
              <MenuItem value={20}>Air</MenuItem>
            </Select>
          </FormControl>
        </div>
        <div className='col-4'>
          <TextField label="Loading port" fullWidth/>
        </div>
        <div className='col-4'>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Zone de destination*</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select">
              <MenuItem value={10}>Sea</MenuItem>
              <MenuItem value={20}>Air</MenuItem>
            </Select>
          </FormControl>        </div>
      </div>
    </div>
  )
}

export default CreateWorkOrderStep1;
