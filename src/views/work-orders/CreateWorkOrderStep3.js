import React from "react";
import Avatar from "@material-ui/core/Avatar";
import {Folder, Trash2, Edit, MessageCircle,Cloud} from "react-feather";

const CreateWorkOrderStep3 = () => {

  return (
    <div className='mt-2 mb-4 px-3'>
      <div>
        <div className='d-flex mb-4'>
          <div className="col-3 pl-5 pr-0 py-0">Document type</div>
          <div className="col-1 p-0">Format</div>
          <div className="col-1 p-0">Size</div>
          <div className="col-2 p-0">Upload date</div>
          <div className="col-3 p-0">Comment</div>
          <div className="col-2 p-0">Actions</div>
        </div>
        {[0, 0, 0, 0].map(elt => <FileItem/>)}
        <div className='height-100 file-item-upload-space d-flex justify-content-center align-items-center mt-4'>
          <div><Cloud/> &nbsp; Drop files here to upload or <span>choose file</span></div>
        </div>
      </div>
    </div>
  )
}

const FileItem = () => {

  const commentTruncate = comment => {
    if (comment.length <= 18)
      return comment
    else return comment.slice(0, 18) + ' ...'
  }
  return (
    <div className='d-flex align-items-center file-item mb-3 pr-3'>
      <div className='col-3 d-flex align-items-center p-0'>
        <Avatar className='mr-3 my-2 ml-2 box-shadow-1' style={{height: 32, width: 32}}>
          <Folder style={{height: 16}}/>
        </Avatar>
        qwertyuiopp
      </div>
      <div className='col-1 p-0'>xxxxx</div>
      <div className='col-1 p-0'>20.8 Mo</div>
      <div className='col-2 p-0'> 28/08/2020 02:19</div>
      <div className='col-3 p-0'>{commentTruncate('i am an example of comment made by')}</div>
      <div
        className='col-2 file-item-action bg-white box-shadow-1 d-flex align-items-center py-2 justify-content-between'>
        <MessageCircle className='mr-2' style={{height: 16}}/>
        <Edit className='mr-2' style={{height: 16}}/>
        <Trash2 style={{height: 16}}/>
      </div>
    </div>
  )
}

export default CreateWorkOrderStep3;
