import React from "react"
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  UncontrolledDropdown,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  ButtonGroup, Button
} from "reactstrap"
import Fab from '@material-ui/core/Fab'
import WorkOrderModel from "./../../models/workOrderModel"
import WorkOrderService from "./../../services/workOrderService"
import {ContextLayout} from "../../utility/context/Layout"
import {AgGridReact} from "ag-grid-react"
import {
  Edit,
  Trash2,
  ChevronDown,
  Clipboard,
  Printer,
  Download,
  Plus, Search,
} from "react-feather"
import classnames from "classnames"
import {history} from "../../history"
import "../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../assets/scss/pages/users.scss"
import CreateWorkOrder from "./CreateWorkOrder";

class WorkOrders extends React.Component {

  handleClickOpen = () => {
    this.setState({createWorkOrderOpen: true});
  };

  handleClickClose = () => {
    this.setState({createWorkOrderOpen: false});
  };

  state = {
    rowData: null,
    pageSize: 20,
    isVisible: true,
    reload: false,
    collapse: false,
    createWorkOrderOpen: false,
    status: "Opened",
    role: "All",
    selectStatus: "All",
    verified: "All",
    department: "All",
    defaultColDef: {
      sortable: true
    },
    workOrdersIndex: 2,
    workOrderType: [
      'Unsubmitted works orders',
      'Submitted works orders',
      'All Work orders'
    ],
    searchVal: "",
    columnDefs: [
      {
        headerName: "N° Transit order",
        field: "id",
        width: 200,
        filter: true,
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true
      },
      {
        headerName: "N° Quotation",
        field: "num_quotation",
        filter: true,
        width: 250,
      },
      {
        headerName: "Ma Ref.",
        field: "reference",
        filter: true,
        width: 250
      },
      {
        headerName: "N° BL",
        field: "blNumber",
        filter: true,
        width: 250
      },
      {
        headerName: "Service",
        field: "service",
        filter: true,
        width: 250
      },
      {
        headerName: "Quotation",
        field: "quotation",
        filter: true,
        width: 250
      },
      {
        headerName: "Date OT",
        field: "country",
        filter: true,
        width: 250
      },
      {
        headerName: "Status",
        field: "status",
        filter: true,
        width: 250,
        cellRendererFramework: params => {
          return params.value === "executed" ? (
            <div className="badge badge-pill badge-light-success col-12 py-2">
              {params.value}
            </div>
          ) : params.value === "rejected" ? (
            <div className="badge badge-pill badge-light-danger col-12 py-2">
              {params.value}
            </div>
          ): params.value === "processing" ? (
            <div className="badge badge-pill badge-light-warning col-12 py-2">
              {params.value}
            </div>
          ) : params.value === "approved" ? (
            <div className="badge badge-pill badge-light-info col-12 py-2">
              {params.value}
            </div>
          ) : null
        }
      },
      {
        headerName: "Actions",
        field: "transactions",
        width: 150,
        cellRendererFramework: () => {
          return (
            <div className="actions cursor-pointer">
              <Edit
                className="mr-75"
                size={16}
                onClick={() => history.push("/app/user/edit")}
              />
              <Trash2
                size={16}
                onClick={() => {
                  let selectedData = this.gridApi.getSelectedRows()
                  this.gridApi.updateRowData({remove: selectedData})
                }}
              />
            </div>
          )
        }
      }
    ]
  }

  async componentDidMount() {
    const res = await new WorkOrderService().getWorkOrdersByUser()
    if (res.status === 200) {
      console.log(res.data);
      this.setState({rowData: res.data?.map(elt => new WorkOrderModel({...elt}).toDataArray())})
    } else
      this.setState({rowData: []})
  }

  filterData = (column, val) => {
    var filter = this.gridApi.getFilterInstance(column)
    var modelObj = null
    if (val !== "all") {
      modelObj = {
        type: "equals",
        filter: val
      }
    }
    filter.setModel(modelObj)
    this.gridApi.onFilterChanged()
  }

  filterSize = val => {
    if (this.gridApi) {
      this.gridApi.paginationSetPageSize(Number(val))
      this.setState({
        pageSize: val
      })
    }
  }
  updateSearchQuery = val => {
    this.setState({
      searchVal: val
    })
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }

  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }

  render() {
    const {rowData, columnDefs, defaultColDef, pageSize} = this.state
    return (
      <Row className="app-quotations-list position-relative">
        <CreateWorkOrder open={this.state.createWorkOrderOpen} setOpen={this.handleClickOpen}
                         setClose={this.handleClickClose}/>
        <Col sm="12">
          <Card
            className={classnames("card-action card-reload", {
              "d-none": this.state.isVisible === false,
              "card-collapsed": this.state.status === "Closed",
              closing: this.state.status === "Closing...",
              opening: this.state.status === "Opening...",
            }, 'box-shadow-0')}
          >
            <CardHeader>
              <div className='col-md-3 col-sm-12 col-lg-4 d-flex'>
                <Fab variant="extended" color="primary" aria-label="add" className='rounded'
                     onClick={this.handleClickOpen}>
                  <Plus/> &nbsp;&nbsp;Create a work order
                </Fab>
              </div>
              <div className='bg-accent-1 col-md-9 col-sm-12 col-lg-8 d-flex justify-content-end'>
                <ButtonGroup className='box-shadow-0 rounded'>
                  {this.state.workOrderType.map((elt, i) => {
                    return (
                      <Button
                      key={i}
                        className={classnames('rounded', 'bg-transparent', 'tabs-options', this.state.workOrdersIndex === i ? 'bottom-bord' : '')}
                        color='whitesmoke'
                        onClick={() => this.setState({quoteIndex: i})}>{elt}
                      </Button>
                    )
                  })}
                </ButtonGroup>
              </div>
              <div className='pl-3 mt-4'>
                <div>Get the set of task or job for a customer, that can be scheduled or assigned to someone. Such an
                  order may be from a customer request or created internally within the organization
                </div>
              </div>
            </CardHeader>
          </Card>
        </Col>
        <Col sm="12">
          <Card>
            <CardBody>
              <div className="ag-theme-material ag-grid-table pb-5 pt-2" style={{height: "calc(100vh - 100px)"}}>
                <div className="ag-grid-actions d-flex justify-content-between flex-wrap mb-3">
                  <div className='d-flex align-items-center'>
                    <div className="sort-dropdown">
                      <UncontrolledDropdown className="ag-dropdown px-3 py-2 ml-3">
                        <DropdownToggle tag="div">
                          1 - {pageSize} of 150
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(20)}
                          >
                            20
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(50)}
                          >
                            50
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(100)}
                          >
                            100
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(150)}
                          >
                            150
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>
                  </div>
                  <div className="filter-actions col-5 d-flex align-items-center justify-content-end">
                    <Search className='mr-3' onClick={() => {
                    }}/>
                    <div className="dropdown actions-dropdown">
                      <UncontrolledButtonDropdown className='d-flex'>
                        <DropdownToggle className="px-2 py-75" color="white">
                          Actions
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="a">
                            <Trash2 size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Delete</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Clipboard size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Archive</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Printer size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Print</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Download size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Excel</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Download size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">PDF</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Download size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">CSV</span>
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledButtonDropdown>
                    </div>
                  </div>
                </div>
                {this.state.rowData !== null ? (
                  <ContextLayout.Consumer>
                    {context => (
                      <AgGridReact
                        gridOptions={{roupUseEntireRow: true}}
                        rowSelection="multiple"
                        defaultColDef={defaultColDef}
                        columnDefs={columnDefs}
                        rowData={rowData}
                        colResizeDefault={"shift"}
                        animateRows={true}
                        floatingFilter={false}
                        pagination={true}
                        pivotPanelShow="always"
                        onSelectionChanged={this.onSelectionChanged}
                        paginationPageSize={pageSize}
                        resizable={true}
                        enableRtl={context.state.direction === "rtl"}
                      />
                    )}
                  </ContextLayout.Consumer>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default WorkOrders
