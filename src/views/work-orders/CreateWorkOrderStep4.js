import React, {useState} from "react";
import {Divider} from "@material-ui/core";
import CustomRadio from "../../components/custom/CustomRadio";

const CreateWorkOrderStep4 = () => {

  const services_to_consider = [
    {text: 'Shipment subject to VAT?', value: useState()},
    {text: 'Local insurance included in the offer?', value: useState()},
    {text: 'Will the goods be packed in a wooden case?', value: useState()},
    {text: 'Collection of the goods included in the offer?', value: useState()},
    {text: 'Cargo Tracking Note included in the offer?', value: useState()},
  ]

  return (
    <div className='mt-2 mb-4 px-3'>
      <div className='text-center my-4 font-medium-4'>
        Kindly choose the services to consider in the scope of work
      </div>
      <Divider light className='m-0'/>
      {services_to_consider.map(elt => {
        return (
          <div>
            <CustomRadio text={elt.text}/>
            <Divider light className='m-0'/>
          </div>
        )
      })}
    </div>
  )
}

export default CreateWorkOrderStep4;
