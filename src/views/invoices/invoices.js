import React from "react"
import {
  Card,
  CardBody,
  CardHeader,
  Row,
  Col,
  UncontrolledDropdown,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  ButtonGroup, Button
} from "reactstrap"
import axios from "axios"
import {ContextLayout} from "../../utility/context/Layout"
import {AgGridReact} from "ag-grid-react"
import {
  Edit,
  Trash2,
  ChevronDown,
  Clipboard,
  Printer,
  Download,
  Search,
} from "react-feather"
import classnames from "classnames"
import {history} from "../../history"
import "../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../assets/scss/pages/users.scss"
import GetQuote from "./getQuote/GetQuote";

class Quotations extends React.Component {

  handleClickOpen = () => {
    const getQuoteOpen = true;
    this.setState({getQuoteOpen});
  };

  handleClickClose = () => {
    const getQuoteOpen = false;
    this.setState({getQuoteOpen});
  };

  state = {
    rowData: null,
    pageSize: 20,
    isVisible: true,
    reload: false,
    isFiltering: false,
    collapse: false,
    getQuoteOpen: false,
    status: "Opened",
    role: "All",
    selectStatus: "All",
    verified: "All",
    department: "All",
    defaultColDef: {
      sortable: true
    },
    quoteIndex: 3,
    quotationType: [
      'Unpaid invoices',
      'Paid invoices',
      'Partially paid invoices',
      'All invoices'
    ],
    searchVal: "",
    columnDefs: [
      {
        headerName: "N° invoice",
        field: "id",
        width: 200,
        filter: true,
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true
      },
      {
        headerName: "N° Transit Orders",
        field: "username",
        filter: true,
        width: 250,
      },
      {
        headerName: "My Reference",
        field: "email",
        filter: true,
        width: 250
      },
      {
        headerName: "Packaging",
        field: "country",
        filter: true,
        width: 250
      },
      {
        headerName: "N° BL",
        field: "email",
        filter: true,
        width: 250
      },
      {
        headerName: "Status",
        field: "status",
        filter: true,
        width: 250,
        cellRendererFramework: params => {
          return params.value === "active" ? (
            <div className="badge badge-pill badge-light-success col-12 py-2">
              {params.value}
            </div>
          ) : params.value === "blocked" ? (
            <div className="badge badge-pill badge-light-danger col-12 py-2">
              {params.value}
            </div>
          ) : params.value === "deactivated" ? (
            <div className="badge badge-pill badge-light-warning col-12 py-2">
              {params.value}
            </div>
          ) : null
        }
      },
      {
        headerName: "Service",
        field: "name",
        filter: true,
        width: 250
      },
      {
        headerName: "Date invoice",
        field: "name",
        filter: true,
        width: 250
      },
      {
        headerName: "Amount",
        field: "name",
        filter: true,
        width: 250
      },
      {
        headerName: "Actions",
        field: "transactions",
        width: 150,
        cellRendererFramework: () => {
          return (
            <div className="actions cursor-pointer">
              <Edit
                className="mr-75"
                size={16}
                onClick={() => history.push("/app/user/edit")}
              />
              <Trash2
                size={16}
                onClick={() => {
                  let selectedData = this.gridApi.getSelectedRows()
                  this.gridApi.updateRowData({remove: selectedData})
                }}
              />
            </div>
          )
        }
      }
    ]
  }

  async componentDidMount() {
    await axios.get("api/users/list").then(response => {
      let rowData = response.data
      this.setState({rowData})
    })
  }

  filterData = (column, val) => {
    var filter = this.gridApi.getFilterInstance(column)
    var modelObj = null
    if (val !== "all") {
      modelObj = {
        type: "equals",
        filter: val
      }
    }
    filter.setModel(modelObj)
    this.gridApi.onFilterChanged()
  }

  filterSize = val => {
    if (this.gridApi) {
      this.gridApi.paginationSetPageSize(Number(val))
      this.setState({
        pageSize: val
      })
    }
  }
  updateSearchQuery = val => {
    this.setState({
      searchVal: val
    })
  }

  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }

  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }

  render() {
    const {rowData, columnDefs, defaultColDef, pageSize} = this.state
    return (
      <Row className="app-quotations-list position-relative">
        <GetQuote open={this.state.getQuoteOpen} setOpen={this.handleClickOpen} setClose={this.handleClickClose}/>
        <Col sm="12">
          <Card
            className={classnames("card-action card-reload", {
              "d-none": this.state.isVisible === false,
              "card-collapsed": this.state.status === "Closed",
              closing: this.state.status === "Closing...",
              opening: this.state.status === "Opening...",
            }, 'box-shadow-0')}
          >
            <CardHeader>
              <div className='col-md-3 col-sm-12 col-lg-4 d-flex'>
              </div>
              <div className='bg-accent-1 col-md-9 col-sm-12 col-lg-8 d-flex justify-content-end'>
                <ButtonGroup className='box-shadow-0 rounded'>
                  {this.state.quotationType.map((elt, i) => {
                    return (
                      <Button
                        className={classnames('rounded', 'bg-transparent', 'tabs-options', this.state.quoteIndex === i ? 'bottom-bord' : '')}
                        color='whitesmoke'
                        onClick={() => this.setState({quoteIndex: i})}>{elt}
                      </Button>
                    )
                  })}
                </ButtonGroup>
              </div>
              <div className='pl-3 mt-4'>
                <div>
                  Get and manage and manage all information related to the quotations hat you have done since the
                  registration. Accessibility available until the management of the quotation's files.
                </div>
              </div>
            </CardHeader>
          </Card>
        </Col>
        <Col sm="12">
          <Card>
            <CardBody>
              <div className="ag-theme-material ag-grid-table pb-5 pt-2" style={{height: "calc(100vh - 100px)"}}>
                <div className="ag-grid-actions d-flex justify-content-between flex-wrap mb-3">
                  <div className='d-flex align-items-center'>
                    <div className="sort-dropdown">
                      <UncontrolledDropdown className="ag-dropdown px-3 py-2 ml-3">
                        <DropdownToggle tag="div">
                          1 - {pageSize} of 150
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="div" onClick={() => this.filterSize(20)}>20</DropdownItem>
                          <DropdownItem tag="div" onClick={() => this.filterSize(50)}>50</DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(100)}
                          >
                            100
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(150)}
                          >
                            150
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>
                  </div>
                  <div className="filter-actions col-5 d-flex align-items-center justify-content-end">
                    <Search className='mr-3' onClick={() => {
                    }}/>
                    <div className="dropdown actions-dropdown">
                      <UncontrolledButtonDropdown className='d-flex'>
                        <DropdownToggle className="px-2 py-75" color="white">
                          Actions
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="a">
                            <Trash2 size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Delete</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Clipboard size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Archive</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Printer size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Print</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Download size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">CSV</span>
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledButtonDropdown>
                    </div>
                  </div>
                </div>
                {this.state.rowData !== null ? (
                  <ContextLayout.Consumer>
                    {context => (
                      <AgGridReact
                        gridOptions={{roupUseEntireRow: true}}
                        rowSelection="multiple"
                        defaultColDef={defaultColDef}
                        columnDefs={columnDefs}
                        rowData={rowData}
                        colResizeDefault={"shift"}
                        animateRows={true}
                        floatingFilter={this.state.isFiltering}
                        pagination={true}
                        pivotPanelShow="always"
                        onSelectionChanged={this.onSelectionChanged}
                        paginationPageSize={pageSize}
                        resizable={true}
                        enableRtl={context.state.direction === "rtl"}
                      />
                    )}
                  </ContextLayout.Consumer>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default Quotations
