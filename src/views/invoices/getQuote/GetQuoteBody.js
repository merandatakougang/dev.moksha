import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Button,
  Typography,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Paper
} from '@material-ui/core';
import GetQuoteStep1 from "./steps/GetQuoteStep1";
import GetQuoteByAirStep2 from "./steps/GetQuoteByAirStep2";
import GetQuoteStep3 from "./steps/GetQuoteStep3";
import GetQuoteBySeaStep2 from "./steps/GetQuoteBySeaStep2";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%'
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

function getSteps() {
  return ['Transaction\'s description', 'Cargo description', 'Scope of work'];
}

export default function GetQuoteBody() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  const [transaction_mode, setTransaction_mode] = useState(null),
    [type_of_transaction, setType_of_transaction] = useState(),
    [delivery_port, setDelivery_port] = useState(),
    [packaging, setPackaging] = useState(),
    [error1, setError1] = useState(false);

  const [containers,setContainer] = useState([0, 0, 0]);

  const [type_of_commodity, setType_of_commodity] = useState(),
    [weight, setWeight] = useState(),
    [length_, setLength] = useState(),
    [width, setWidth] = useState(),
    [height, setHeight] = useState(),
    [origin, setOrigin] = useState(),
    [cfr, setCfr] = useState(),
    [currency, setCurrency] = useState(),
    [error2, setError2] = useState(false);

  let getQuote = {}

  const getStepContent = step => {
    switch (step) {
      case 0:
        return (
          <GetQuoteStep1
            classes={classes}
            transaction_mode={transaction_mode}
            setTransaction_mode={setTransaction_mode}
            type_of_transaction={type_of_transaction}
            setType_of_transaction={setType_of_transaction}
            delivery_port={delivery_port}
            setDelivery_port={setDelivery_port}
            packaging={packaging}
            setPackaging={setPackaging}
            error={error1}
          />);
      case 1:
        return transaction_mode === 2 ?
          <GetQuoteByAirStep2
            classes={classes}
            type_of_commodity={type_of_commodity}
            setType_of_commodity={setType_of_commodity}
            weight={weight}
            setWeight={setWeight}
            length_={length_}
            setLength={setLength}
            width={width}
            setWidth={setWidth}
            height={height}
            setHeight={setHeight}
            origin={origin}
            setOrigin={setOrigin}
            cfr={cfr}
            setCfr={setCfr}
            currency={currency}
            setCurrency={setCurrency}
            error={error2}
          /> : transaction_mode === 1 ? (
            <GetQuoteBySeaStep2/>
          ) : <div/>
      case 2:
        return <GetQuoteStep3 classes={classes} form={getQuote}/>
      default:
        return 'Unknown step';
    }
  }

  const steps = getSteps();

  const handleNext = () => {
    switch (activeStep) {
      case 0:
        if (transaction_mode && type_of_transaction && delivery_port && packaging) {
          setActiveStep((prevActiveStep) => prevActiveStep + 1);
          setError1(false)
        } else {
          setError1(true)
        }
        break;
      case 1:
        if (transaction_mode === 2) {
          if (type_of_commodity && weight && length_ && height && width && currency && origin && cfr) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
            setError2(false)
          } else {
            setError2(true)
          }
        } else if (transaction_mode === 1) {
          setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
        break;
      default:
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };


  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(index)}
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
        </Paper>
      )}
    </div>
  );
}
