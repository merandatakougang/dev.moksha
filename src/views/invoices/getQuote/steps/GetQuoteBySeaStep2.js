import React, {useState} from "react";
import Avatar from "@material-ui/core/Avatar";
import {Folder, Copy, Trash2, Edit, MessageCircle, Upload, Cloud, Check} from "react-feather";
import {Input} from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import IOSSwitch from "../../../../components/custom/IOSSwitch";

const CreateWorkOrderStep3 = () => {

  const [containers,setContainer] = useState([0, 0, 0]),
    ContainerItem = () => {

      return (
        <div className='d-flex align-items-center file-item mb-3 pr-3'>
          <div className='col-3 d-flex align-items-center p-0'>
            <Avatar className='mr-3 my-2 ml-2 box-shadow-1' style={{height: 32, width: 32}}>
              <Folder style={{height: 16}}/>
            </Avatar>
            qwertyuiopp
          </div>
          <div className='col-3 p-0'>xxxxx</div>
          <div className='col-2 py-0 pl-4 pr-0'> 30.000</div>
          <div className='col-2 p-0'>
            <div
              className="bg-reddit rounded mx-3 text-white d-flex align-items-center justify-content-center width-50 font-small-1 py-1">YES
            </div>
          </div>
          <div
            className='col-2 file-item-action bg-white box-shadow-1 d-flex align-items-center py-2 justify-content-between'>
            <Copy className='mr-2' style={{height: 16}}/>
            <Edit className='mr-2 accent-3' style={{height: 16}}/>
            <Trash2 style={{height: 16}}/>
          </div>
        </div>
      )
    },
    ContainerItemEmpty = () => {

      return (
        <div className='d-flex align-items-center file-item mb-3 pr-3'>
          <div className='col-3 d-flex align-items-center p-0'>
            <Avatar className='mr-3 my-2 ml-2 box-shadow-1' style={{height: 32, width: 32}}>
              &nbsp;
            </Avatar>
            <InputBase placeholder="Type of container" className='font-small-3 font-italic'/>
          </div>
          <div className='col-3 p-0'>
            <InputBase placeholder="Commodity type" className='font-small-3 font-italic'/>
          </div>
          <div className='col-2 py-0 pl-4 pr-0'>
            <InputBase placeholder="weight" className='font-small-3 font-italic'/>
          </div>
          <div className='col-2 p-0'>
            <div className="ml-4 d-flex align-items-center justify-content-center width-50">
              <FormControlLabel control={<IOSSwitch name="checkedB"/>}/>
            </div>
          </div>
          <div onClick={()=>{
            const arr = [...containers,0]
            setContainer(arr)
          }}
               className='col-2 file-item-action bg-white box-shadow-1 d-flex align-items-center py-2 justify-content-center'>
            <Check style={{height: 16}}/>
            <div>save</div>
          </div>
        </div>
      )
    }

  return (
    <div className='mt-2 mb-4 px-3'>
      <div>
        <div className='d-flex mb-4'>
          <div className="col-3 pl-5 pr-0 py-0">Type of container</div>
          <div className="col-3 p-0">Type of commodity</div>
          <div className="col-2 p-0"> Weight (Kg)</div>
          <div className="col-2 p-0">Dangerous ?</div>
          <div className="col-2 p-0">Actions</div>
        </div>
        {containers.map(elt => <ContainerItem/>)}
        <ContainerItemEmpty/>
      </div>
    </div>
  )
}

export default CreateWorkOrderStep3;
