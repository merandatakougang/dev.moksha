import React, {useState} from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";

const GetQuoteStep1 = props => {

  const classes = props.classes
  return (
    <div className='mt-2'>
      <div className='d-flex mb-3 col-8'>
        <FormControl className={classes.formControl + ' col-6'} error={props.error&&!props.transaction_mode}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>Transaction mode*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.transaction_mode}
            onChange={event => props.setTransaction_mode(event.target.value)}>
            <MenuItem value={1}>Sea</MenuItem>
            <MenuItem value={2}>Air</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl + ' col-6'} error={props.error&&!props.type_of_transaction}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>Type of transaction*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.type_of_transaction}
            onChange={event => props.setType_of_transaction(event.target.value)}>
            <MenuItem value={1}>Import</MenuItem>
            <MenuItem value={2}>Export</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className='d-flex mb-4 col-8'>
        <FormControl className={classes.formControl + ' col-6'} error={props.error&&!props.delivery_port}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>
            {props.delivery_port === 1 ? 'Discharging' : props.delivery_port === 2 ? 'Loading' : 'Delivery'} port*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.delivery_port}
            onChange={event => props.setDelivery_port(event.target.value)}>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
          </Select>
        </FormControl>
        <FormControl className={classes.formControl + ' col-6'} error={props.error&&!props.packaging}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>Packaging*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.packaging}
            onChange={event => props.setPackaging(event.target.value)}>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </div>
    </div>
  )
}

export default GetQuoteStep1;
