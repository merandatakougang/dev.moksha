import React from "react";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core";

const GetQuoteStep1 = props => {

  const classes = props.classes
  return (
    <div className='mt-2'>
      <div className='d-flex mb-3 col-8'>
        <FormControl className={classes.formControl + ' col-6'} error={props.error && !props.transaction_mode}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>Transaction mode*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.transaction_mode}
            onChange={event => {
              const tmode_v = props.transportModesArray.find(elt => elt.key === event.target.value)
              props.setTransaction_mode_value(tmode_v.value)
              props.setDelivery_port(null)
              props.setPackaging(null)
              props.setTransaction_mode(event.target.value)
            }}>
            {props.transportModesArray?.map((elt, i) => <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
          </Select>
        </FormControl>
        <FormControl className={classes.formControl + ' col-6'} error={props.error && !props.type_of_transaction}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>Type of transaction*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.type_of_transaction}
            onChange={event => {
              props.setIsExport(event.target.value)
              props.setType_of_transaction(event.target.value)
            }}>
            {props.transactionTypesArray?.map((elt, i) => <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
      <div className='d-flex mb-4 col-8'>
        <FormControl className={classes.formControl + ' col-6'} error={props.error && !props.delivery_port}>
          <InputLabel id="demo-simple-select-label" className='ml-3'>
            {props.type_of_transaction === 1 ? 'Discharging' : props.type_of_transaction === 2 ? 'Loading' : 'Delivery'} port*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.delivery_port}
            onChange={event => props.setDelivery_port(event.target.value)}>
            {props.dischargeHarborsArray?.filter(elt => elt.transportMode === props.transaction_mode_value).map((elt, i) =>
              <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
          </Select>
        </FormControl>
        {props.transaction_mode_value === 'Sea' ?
          <FormControl className={classes.formControl + ' col-6'} error={props.error && !props.packaging}>
            <InputLabel id="demo-simple-select-label" className='ml-3'>Packaging*</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={props.packaging}
              onChange={event => props.setPackaging(event.target.value)}>
              {props.packagingArray?.map((elt, i) => <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
            </Select>
          </FormControl> : null}
      </div>
    </div>
  )
}

export default GetQuoteStep1;
