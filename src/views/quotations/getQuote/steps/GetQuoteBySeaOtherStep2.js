import React from "react";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";

const GetQuoteBySeaOtherStep2 = props => {

  return (
    <div className='mt-2'>
      <div className='d-flex mb-4 col-8'>
        <TextField
          className='col-6 ml-0 pl-0'
          error={props.error && !props.cfr}
          label="Weight"
          required
          type='number'
          value={props.weight}
          onChange={event => props.setWeight(event.target.value)}/>

        <FormControl className='col-6 pl-0 ml-0' error={props.error && !props.origin}>
          <InputLabel id="demo-simple-select-label">Final destination*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.final_destination}
            onChange={event => props.setFinal_destination(event.target.value)}>
            {props.final_destinationArray?.map((elt, i) =><MenuItem key={i} value={elt.id}>{elt.name}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
      <div className='d-flex mb-4 col-8'>
        <TextField
          className='col-6 ml-0 pl-0'
          error={props.error && !props.type_of_commodity}
          label="Type of commodity"
          required
          value={props.type_of_commodity}
          onChange={event => props.setType_of_commodity(event.target.value)}/>
        <TextField
          className='col-4 ml-0 pl-0'
          error={props.error && !props.cfr}
          label="CFR Value*"
          type='number'
          value={props.cfr}
          onChange={event => props.setCfr(event.target.value)}/>

        <FormControl className='col-2 px-0 mx-0' error={props.error && !props.currency}>
          <InputLabel id="demo-simple-select-label">CUR*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.currency}
            onChange={event => props.setCurrency(event.target.value)}
          >
            {props.currenciesArray?.map((elt, i) => <MenuItem key={i} value={elt.value}>{elt.key}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
    </div>
  )
}

export default GetQuoteBySeaOtherStep2;
