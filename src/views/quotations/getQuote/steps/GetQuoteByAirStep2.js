import React from "react";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@material-ui/core";

const GetQuoteByAirStep2 = props => {

  return (
    <div className='mt-2'>
      <div className='d-flex mb-3 col-8'>
        <TextField
          label="Type of commodity*"
          fullWidth
          error={props.error && !props.type_of_commodity}
          value={props.type_of_commodity}
          onChange={event => props.setType_of_commodity(event.target.value)}/>
      </div>
      <div className='d-flex mb-3 col-8'>
        <TextField
          label="Weight*"
          className='col-3 ml-0 pl-0'
          error={props.error && !props.weight}
          value={props.weight}
          type='number'
          onChange={event => props.setWeight(event.target.value)}/>
        <TextField
          className='col-3 ml-0 pl-0'
          error={props.error && !props.length_}
          label="Length*"
          value={props.length_}
          type='number'
          onChange={event => props.setLength(event.target.value)}/>
        <TextField
          className='col-3 ml-0 pl-0'
          error={props.error && !props.width}
          label="Width*"
          type='number'
          value={props.width}
          onChange={event => props.setWidth(event.target.value)}/>
        <TextField
          className='col-3 mx-0 px-0'
          error={props.error && !props.height}
          label="Height*"
          type='number'
          value={props.height}
          onChange={event => props.setHeight(event.target.value)}/>
      </div>
      <div className='d-flex mb-4 col-8'>
        <FormControl className='col-6 pl-0 ml-0' error={props.error && !props.origin}>
          <InputLabel id="demo-simple-select-label">Origin of the goods*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.origin}
            onChange={event => props.setOrigin(event.target.value)}
          >
            {props.deliveryPlacesArray?.filter(elt => elt.transactionType.id === props.getQUote?.type_of_transaction && elt.packaging === props.getQUote?.packaging).map((elt, i) =>
              <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
          </Select>
        </FormControl>

        <TextField
          className='col-4 ml-0 pl-0'
          error={props.error && !props.cfr}
          label="CFR Value*"
          type='number'
          value={props.cfr}
          onChange={event => props.setCfr(event.target.value)}/>

        <FormControl className='col-2 px-0 mx-0' error={props.error && !props.currency}>
          <InputLabel id="demo-simple-select-label">CUR*</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.currency}
            onChange={event => props.setCurrency(event.target.value)}
          >
            {props.currenciesArray?.map((elt, i) => <MenuItem key={i} value={elt.value}>{elt.key}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
    </div>
  )
}

export default GetQuoteByAirStep2;
