import React, {useState} from "react";
import Avatar from "@material-ui/core/Avatar";
import {Folder, Copy, Trash2, Edit, Check} from "react-feather";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import IOSSwitch from "../../../../components/custom/IOSSwitch";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import Select from "@material-ui/core/Select";
import TextField from "@material-ui/core/TextField";
import Popper from "@material-ui/core/Popper";
import Paper from "@material-ui/core/Paper";
import Fade from "@material-ui/core/Fade";

const CreateWorkOrderStep3 = (props) => {

  const ContainerItem = (prop) => {
      const [anchorEl, setAnchorEl] = useState(null);
      const [open, setOpen] = useState(false);

      const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        setOpen(true);
      };

      let object = {
        tc: props.quotationInfo?.containerTypes.find(elt => elt.key === prop.item?.tc),
        commodityType: prop.item.type_of_commodity,
        weight: prop.item.weight,
        isDangerous: prop.item.isDangerous
      }


      return (
        <div className='d-flex align-items-center file-item mb-3 pr-3'>
          <Popper open={open} anchorEl={anchorEl} placement='bottom-end' transition>
            {({TransitionProps}) => (
              <Fade {...TransitionProps} timeout={350}>
                <Paper>
                  <Typography>The content of the Popper.</Typography>
                </Paper>
              </Fade>
            )}
          </Popper>
          <div className='col-3 d-flex align-items-center p-0'>
            <Avatar className='mr-3 my-2 ml-2 box-shadow-1' style={{height: 32, width: 32}}>
              <Folder style={{height: 16}}/>
            </Avatar>
            {object.tc.value}
          </div>
          <div className='col-3 p-0'>{object.commodityType}</div>
          <div className='col-2 py-0 pl-4 pr-0'>{object.weight}</div>
          <div className='col-2 p-0'>
            <div
              className="bg-reddit rounded mx-3 text-white d-flex align-items-center justify-content-center width-50 font-small-1 py-1">{object.isDangerous ? 'YES' : 'NO'}</div>
          </div>
          <div
            className='col-2 file-item-action bg-white box-shadow-1 d-flex align-items-center py-2 justify-content-between'>
            <Copy className='mr-2' style={{height: 16}} onClick={handleClick}/>
            <Edit className='mr-2 accent-3' style={{height: 16}}/>
            <Trash2 style={{height: 16}}/>
          </div>
        </div>
      )
    },
    ContainerItemEmpty = (props) => {
      const [tc, setTc] = useState()
      const [weight, setWeight] = useState()
      const [isDangerous, setIsDangerous] = useState(false)
      const [type_of_commodity, setType_of_commodity] = useState()
      return (
        <div className='d-flex align-items-center file-item mb-3 pr-3 py-3'>
          <div className='col-3 d-flex align-items-center p-0'>
            <Avatar className='mr-3 my-2 ml-2 box-shadow-1' style={{height: 32, width: 32}}>
              &nbsp;
            </Avatar>
            <FormControl fullWidth className='mx-3'>
              <InputLabel className='font-italic font-small-3'>TC</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={tc}
                onChange={event => setTc(event.target.value)}
              >
                {props.containerTypes?.map((elt, i) => <MenuItem key={i} value={elt.key}>{elt.value}</MenuItem>)}
              </Select>
            </FormControl>
          </div>
          <div className='col-3 p-0'>
            <FormControl fullWidth className='mx-3'>
              <InputLabel className='font-italic font-small-3'>Commodity type</InputLabel>
              <Select
                value={type_of_commodity}
                onChange={ev => setType_of_commodity(ev.target.value)}>
                {props.commodities?.map((elt, i) =>
                  elt?.value.filter(r => r.transportMode.id === props.transport_mode && r.transactionType.id === props.type_of_transaction && r.packaging.id === props.packaging && r.containerType.id === tc)
                    .map(e => <MenuItem value={e.key}>{e.value}</MenuItem>)
                )}
              </Select>
            </FormControl>
          </div>
          <div className='col-2 py-0 pl-4 pr-0'>
            <TextField id="standard-basic" label="weight" type='number' className='font-small-3 font-italic'
                       value={weight} onChange={ev => setWeight(ev.target.value)}/>
          </div>
          <div className='col-2 p-0'>
            <div className="ml-4 d-flex align-items-center justify-content-center width-50">
              <FormControlLabel
                control={<IOSSwitch name="checkedB" isDangerous={isDangerous} setIsDangerous={setIsDangerous}/>}/>
            </div>
          </div>
          <div onClick={() => {
            const arr = [...props.containers,
              {tc, type_of_commodity, weight, isDangerous}]
            props.setContainers(arr)
          }}
               className='col-2 file-item-action bg-white box-shadow-1 d-flex align-items-center py-2 justify-content-center'>
            <Check style={{height: 16}}/>
            <div>save</div>
          </div>
        </div>
      )
    }

  return (
    <div className='mt-2 mb-4 px-3'>
      <div>
        <div className='d-flex mb-4 col-12'>
          <TextField
            className='col-4 ml-0 pl-0'
            error={props.error && !props.cfr}
            label="CFR Value*"
            type='number'
            value={props.cfr}
            onChange={event => props.setCfr(event.target.value)}/>

          <FormControl className='col-2 pl-0 ml-0' error={props.error && !props.currency}>
            <InputLabel id="demo-simple-select-label">CUR*</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={props.currency}
              onChange={event => props.setCurrency(event.target.value)}
            >
              {props.currenciesArray?.map((elt, i) => <MenuItem key={i} value={elt.value}>{elt.key}</MenuItem>)}
            </Select>
          </FormControl>
          {props.isExport === 2 ?
            <FormControl className='col-6 px-0 mx-0' error={props.error && !props.currency}>
              <InputLabel id="demo-simple-select-label">Country of origin*</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={props.country_of_origin}
                onChange={event => props.setCountry_of_origin(event.target.value)}
              >
                {props.country_of_originArray?.map((elt, i) => <MenuItem key={i} value={elt.id}>{elt.name}</MenuItem>)}
              </Select>
            </FormControl> : null}
        </div>
        <div className='d-flex mb-4'>
          <div className="col-3 pl-5 pr-0 py-0">Type of container</div>
          <div className="col-3 p-0">Type of commodity</div>
          <div className="col-2 p-0"> Weight (Kg)</div>
          <div className="col-2 p-0">Dangerous ?</div>
          <div className="col-2 p-0">Actions</div>
        </div>
        {props.containers.map((elt, i) => <ContainerItem key={i} item={elt}/>)}
        <ContainerItemEmpty
          setContainers={props.setContainers}
          containers={props.containers}
          containerTypes={props.containerTypes}
          transport_mode={props.transaction_mode}
          type_of_transaction={props.type_of_transaction}
          packaging={props.packaging}
          commodities={props.commodities}/>
      </div>
    </div>
  )
}

export default CreateWorkOrderStep3;
