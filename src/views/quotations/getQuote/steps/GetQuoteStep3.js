import React, {useState} from "react";
import {Divider} from "@material-ui/core";
import CustomRadio from "../../../../components/custom/CustomRadio";

const GetQuoteStep3 = props => {

  const services_to_consider = [
    {text: 'Shipment subject to VAT?', value: props.isTva, setValue: props.setIsTva, show: true},
    {
      text: 'Local insurance included in the offer?',
      value: props.isLocalInsurance,
      setValue: props.setIsLocalInsurance,
      show: [3, 4, 1].includes(props.packaging)
    },
    {
      text: 'Marine Insurance included in the offer?',
      value: props.isWoodenCase,
      setValue: props.transport_mode === 1,
      show: [1, 4].includes(props.packaging) && props.type_of_transaction === 2
    },
    {
      text: 'Will this shipment be subject to phytosanitary inspection?',
      value: props.isTva,
      setValue: props.setIsTva,
      show: props.containers.find(elt => elt.isDangerous)
    },
    {
      text: 'Unloading the goods out of the truck included in the offer ?',
      value: props.isTva,
      setValue: props.setIsTva,
      show: props.packaging === 1
    },
    {
      text: 'Positioning of empty containers included in the offer ?',
      value: props.isTva,
      setValue: props.setIsTva,
      show: props.packaging === 1
    },
    {
      text: 'Stuffing included in the offer ?',
      value: props.isTva,
      setValue: props.setIsTva,
      show: props.packaging === 1
    },
    {text: 'Will the goods be packed in a wooden case?', value: props.isWoodenCase, setValue: props.setIsWoodenCase},
    {
      text: 'Cargo Tracking Note included in the offer?',
      value: props.isEtcn,
      setValue: props.setIsEtcn,
      show: [3, 4,1].includes(props.packaging)
    },
    {
      text: 'Delivery to final destination included in the offer?',
      value: props.isEtcn,
      setValue: props.setIsEtcn,
      show: props.packaging === 2 || (props.packaging === 1 && props.type_of_transaction === 1)
    },
    {
      text: 'Transfert of the goods to the port included in the offer ?',
      value: props.isEtcn,
      setValue: props.setIsEtcn,
      show: props.packaging === 4 && props.type_of_transaction === 2
    },
    {
      text: 'Collection of the goods included in the offer?',
      value: props.isCollection,
      setValue: props.setIsCollection
    },
  ]

  return (
    <div className='mt-2 mb-4 px-3'>
      <div className='text-center my-4 font-medium-4'>
        Kindly choose the services to consider in the scope of work
      </div>
      <Divider light className='m-0'/>
      {services_to_consider.map(elt => {
        return (
          <div>
            <CustomRadio text={elt.text} setValue={elt.setValue} value={elt.value}/>
            <Divider light className='m-0'/>
          </div>
        )
      })}
    </div>
  )
}

export default GetQuoteStep3;
