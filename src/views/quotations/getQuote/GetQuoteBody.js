import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {
  Button,
  Typography,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Paper
} from '@material-ui/core';
import GetQuoteStep1 from "./steps/GetQuoteStep1";
import GetQuoteByAirStep2 from "./steps/GetQuoteByAirStep2";
import GetQuoteStep3 from "./steps/GetQuoteStep3";
import GetQuoteBySeaStep2 from "./steps/GetQuoteBySeaStep2";
import GetQuoteBySeaOtherStep2 from "./steps/GetQuoteBySeaOtherStep2";

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    height: '100%'
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    padding: theme.spacing(3),
  },
}));

function getSteps() {
  return ['Transaction\'s description', 'Cargo description', 'Scope of work'];
}

// activeCountries: (3) [{…}, {…}, {…}]
// containerTypes: (4) [{…}, {…}, {…}, {…}]
// countries: (226) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}, …]
// dischargeHarbors: (4) [{…}, {…}, {…}, {…}]
// inOutCountries: (8) [{…}, {…}, {…}, {…}, {…}, {…}, {…}, {…}]
// packaging: (4) [{…}, {…}, {…}, {…}]
// transportRoutes: []

export default function GetQuoteBody(props) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  console.log(props.quotationInfo)

  const [transaction_mode, setTransaction_mode] = useState(null),
    [type_of_transaction, setType_of_transaction] = useState(),
    [getQuote, setGetQuote] = useState(),
    [transaction_mode_value, setTransaction_mode_value] = useState(),
    [delivery_port, setDelivery_port] = useState(),
    [packaging, setPackaging] = useState(),
    [error1, setError1] = useState(false);

  const [containers, setContainer] = useState([]);

  const [type_of_commodity, setType_of_commodity] = useState(),
    [weight, setWeight] = useState(),
    [final_destination, setFinal_destination] = useState(),
    [length_, setLength] = useState(),
    [width, setWidth] = useState(),
    [height, setHeight] = useState(),
    [origin, setOrigin] = useState(),
    [cfr, setCfr] = useState(),
    [currency, setCurrency] = useState(),
    [error2, setError2] = useState(false),
    [country_of_origin, setCountry_of_origin] = useState();

  const [isTva, setIsTva] = useState(null),
    [isLocalInsurance, setIsLocalInsurance] = useState(null),
    [isUnloadingContainer, setIsUnloadingContainer] = useState(null),
    [isPositioningContainer, setIsPositioningContainer] = useState(null),
    [isStuffing, setIsStuffing] = useState(null),
    [isWoodenCase, setIsWoodenCase] = useState(null),
    [isCollection, setIsCollection] = useState(null),
    [isEtcn, setIsEtcn] = useState(null),
    [isExport, setIsExport] = useState(null),
    saveQuote = () => {
      let d = {
        "serviceModeId": transaction_mode,
        "dischargeHarborId": delivery_port,
        "serviceModeSubServiceId": 2,
        "isExport": 1,
        "isTransport": null,
        "isEtcn": isEtcn,
        "isAssurTrans": null,
        "isLocalInsurance": isLocalInsurance,
        "isTva": isTva,
        "isWoodenCase": isWoodenCase,
        "isSeaInsurance": null,
        "isCollection": isCollection,
        "cfrValue": cfr,
        "creatorId": null,
        "commodityWeight": weight,
        "commodityDimensionsL": length_,
        "commodityDimensionsW": width,
        "commodityDimensionsH": height,
        "commodityPlaceId": null,
        "commodityId": null,
        "commodityCustonCommodityName": type_of_commodity,
        "commodityInOutCountryId": 40,
        "quotationValue": null,
        "commodityOriginId": origin,
        "isPhytosanitaryInspection": null,
        "isStuffing": isStuffing,
        "isPositioningContainer": isPositioningContainer,
        "isUnloadingContainer": isUnloadingContainer
      }
    }


  const getStepContent = step => {
    switch (step) {
      case 0:
        return (
          <GetQuoteStep1
            classes={classes}
            transaction_mode={transaction_mode}
            setTransaction_mode={setTransaction_mode}
            type_of_transaction={type_of_transaction}
            setTransaction_mode_value={setTransaction_mode_value}
            transaction_mode_value={transaction_mode_value}
            setType_of_transaction={setType_of_transaction}
            delivery_port={delivery_port}
            setDelivery_port={setDelivery_port}
            packaging={packaging}
            setPackaging={setPackaging}
            setIsExport={setIsExport}
            error={error1}
            transportModesArray={props.quotationInfo?.transportModes}
            transactionTypesArray={props.quotationInfo?.transactionTypes}
            dischargeHarborsArray={props.quotationInfo?.dischargeHarbors}
            packagingArray={props.quotationInfo?.packaging}
          />);
      case 1:
        return transaction_mode === 2 || packaging === 2 ?
          <GetQuoteByAirStep2
            getQUote={getQuote}
            classes={classes}
            type_of_commodity={type_of_commodity}
            setType_of_commodity={setType_of_commodity}
            weight={weight}
            setWeight={setWeight}
            length_={length_}
            setLength={setLength}
            width={width}
            setWidth={setWidth}
            height={height}
            setHeight={setHeight}
            origin={origin}
            setOrigin={setOrigin}
            cfr={cfr}
            setCfr={setCfr}
            currency={currency}
            setCurrency={setCurrency}
            error={error2}
            deliveryPlacesArray={props.quotationInfo?.deliveryPlaces}
            currenciesArray={props.quotationInfo?.currencies}
          /> : packaging === 3 || packaging === 4 ? (
            <GetQuoteBySeaOtherStep2
              getQUote={getQuote}
              type_of_commodity={type_of_commodity}
              setType_of_commodity={setType_of_commodity}
              weight={weight}
              setWeight={setWeight}
              final_destination={final_destination}
              final_destinationArray={props.quotationInfo?.inOutCountries}
              setFinal_destination={setFinal_destination}
              cfr={cfr}
              setCfr={setCfr}
              currency={currency}
              setCurrency={setCurrency}
              error={error2}
              deliveryPlacesArray={props.quotationInfo?.deliveryPlaces}
              currenciesArray={props.quotationInfo?.currencies}/>
          ) : transaction_mode === 1 ? (
            <GetQuoteBySeaStep2
              containers={containers}
              isExport={isExport}
              setContainers={setContainer}
              country_of_origin={country_of_origin}
              setCountry_of_origin={setCountry_of_origin}
              quotationInfo={props.quotationInfo}
              containerTypes={props.quotationInfo?.containerTypes}
              country_of_originArray={props.quotationInfo?.inOutCountries}
              currenciesArray={props.quotationInfo?.currencies}
              commodities={props.quotationInfo?.commodities}
              transaction_mode={transaction_mode}
              type_of_transaction={type_of_transaction}
              packaging={packaging}/>
          ) : null
      case 2:
        return <GetQuoteStep3
          classes={classes}
          getQUote={getQuote}
          isTva={isTva}
          isLocalInsurance={isLocalInsurance}
          isWoodenCase={isWoodenCase}
          isCollection={isCollection}
          isEtcn={isEtcn}
          setIsTva={setIsTva}
          setIsLocalInsurance={setIsLocalInsurance}
          setIsWoodenCase={setIsWoodenCase}
          setIsCollection={setIsCollection}
          setIsEtcn={setIsEtcn}
        />
      default:
        return 'Unknown step';
    }
  }

  const steps = getSteps();

  const handleNext = () => {
    switch (activeStep) {
      case 0:
        if (transaction_mode && type_of_transaction && delivery_port && ((transaction_mode_value === 'Sea' && packaging) || transaction_mode_value !== 'Sea')) {
          setGetQuote({
            transaction_mode,
            type_of_transaction,
            delivery_port,
            packaging
          })
          console.log({
            transaction_mode,
            type_of_transaction,
            delivery_port,
            packaging
          })
          setActiveStep((prevActiveStep) => prevActiveStep + 1);
          setError1(false)
        } else {
          setError1(true)
        }
        break;
      case 1:
        if (transaction_mode === 2) {
          if (type_of_commodity && weight && length_ && height && width && currency && origin && cfr) {
            setActiveStep((prevActiveStep) => prevActiveStep + 1);
            setError2(false)
          } else {
            setError2(true)
          }
        } else if (transaction_mode === 1) {
          setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
        break;
      default:
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };


  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(index)}
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBack}
                    className={classes.button}
                  >
                    Back
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNext}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>
          <Typography>All steps completed - you&apos;re finished</Typography>
          <Button onClick={handleReset} className={classes.button}>
            Reset
          </Button>
          <Button onClick={() => setActiveStep(activeStep - 1)} className={classes.button}>
            Back
          </Button>
          <Button onClick={saveQuote} className={classes.button}>
            Save
          </Button>
        </Paper>
      )}
    </div>
  );
}
