import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import GetQuoteBody from "./GetQuoteBody";
import CustumBackdrop from "../../../components/custom/CustomBackdrop";

const useStyles = makeStyles((theme) => ({
  dialogContainer: {
    height: '100vh'
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
}));

const GetQuote = props => {
  const classes = useStyles(),
    fullWidth = true;

  return (
    <React.Fragment>
      <Dialog
        fullWidth={fullWidth}
        scroll={'paper'}
        maxWidth={'md'}
        open={props.open}
        aria-labelledby="max-width-dialog-title"
      >
        <CustumBackdrop isOpen={props.isOpenGetQuote}/>
        <DialogTitle id="max-width-dialog-title">Get a quote</DialogTitle>
        <DialogContent className={classes.dialogContainer}>
          <GetQuoteBody quotationInfo={props.quotationInfo}/>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={props.setClose}
            color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
export default GetQuote;
