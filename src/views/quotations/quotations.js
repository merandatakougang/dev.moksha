import React from "react"
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Row,
  Col,
  UncontrolledDropdown,
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  ButtonGroup, Button
} from "reactstrap"
import Fab from '@material-ui/core/Fab'
import {ContextLayout} from "../../utility/context/Layout"
import {AgGridReact} from "ag-grid-react"
import {
  Edit,
  Trash2,
  ChevronDown,
  Clipboard,
  Printer,
  Download,
  Plus, Search,
} from "react-feather"
import classnames from "classnames"
import {history} from "../../history"
import "../../assets/scss/plugins/tables/_agGridStyleOverride.scss"
import "../../assets/scss/pages/users.scss"
import GetQuote from "./getQuote/GetQuote";
import QuotationService from "../../services/quotationService";
import QuotationsModel from "../../models/quotationsModel";
import IconButton from "@material-ui/core/IconButton";
import {Refresh} from "@material-ui/icons";
import CustomBackdrop from "../../components/custom/CustomBackdrop";

class Quotations extends React.Component {

  handleClickOpen = async () => {
    const getQuoteOpen = true;
    this.setState({getQuoteOpen});
    this.setState({isOpenGetQuote: true});
    let resultInfo = await this.quotationService.getQuotationInfo()
    this.setState({isOpenGetQuote: false});
    console.log(resultInfo)
    if (resultInfo.status === 200)
      this.setState({quotationInfo: resultInfo.data})
  };

  handleClickClose = () => {
    const getQuoteOpen = false;
    this.setState({getQuoteOpen});
  };

  quotationService = new QuotationService()

  state = {
    isOpenGetQuote: false,
    quotationInfo: {},
    isOpen: false,
    rowData: null,
    pageSize: 20,
    isVisible: true,
    reload: false,
    isFiltering: false,
    collapse: false,
    getQuoteOpen: false,
    status: "Opened",
    role: "All",
    selectStatus: "All",
    verified: "All",
    department: "All",
    defaultColDef: {
      sortable: true
    },
    quoteIndex: 3,
    quotationType: [
      'Quote to approves',
      'Accepted quotes',
      'Rejected quotes',
      'Expired quotes',
      'All quotes'
    ],
    searchVal: "",
    columnDefs: [
      {
        headerName: "N°",
        field: "num",
        width: 200,
        filter: true,
        checkboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true,
        headerCheckboxSelection: true
      },
      {
        headerName: "Quotation date",
        field: "date",
        filter: true,
        width: 250,
        cellRendererFramework: params => new Date(params.value).toLocaleString()
      },
      {
        headerName: "Quotation amount",
        field: "amount",
        filter: true,
        width: 250,
        cellRendererFramework: params => params.value + ' FCFA'
      },
      {
        headerName: "Service",
        field: "service",
        filter: true,
        width: 250,
      },
      {
        headerName: "Packaging",
        field: "packaging",
        filter: true,
        width: 250
      },
      {
        headerName: "Status",
        field: "status",
        filter: true,
        width: 250,
        cellRendererFramework: params => {
          return params.value === "executed" ? (
            <div className="badge badge-pill badge-light-success col-12 py-2">
              {params.value}
            </div>
          ) : params.value === "rejected" ? (
            <div className="badge badge-pill badge-light-danger col-12 py-2">
              {params.value}
            </div>
          ) : null
        }
      },
      {
        headerName: "Actions",
        field: "transactions",
        width: 150,
        cellRendererFramework: () => {
          return (
            <div className="actions cursor-pointer">
              <Edit
                className="mr-2"
                size={16}
                onClick={() => history.push("/app/user/edit")}
              />
              <Trash2
                size={16}
                onClick={() => {
                  let selectedData = this.gridApi.getSelectedRows()
                  this.gridApi.updateRowData({remove: selectedData})
                }}
              />
            </div>
          )
        }
      }
    ]
  }


  loadDataArray = async () => {
    this.setState({isOpen: true})
    const res = await this.quotationService.getQuotationsByUser()
    this.setState({isOpen: false})
    if (res.status === 200) {
      this.setState({rowData: res.data?.map(elt => new QuotationsModel({...elt}).toDataArray())})
    } else
      this.setState({rowData: []})
  }

  async componentDidMount() {
    await this.loadDataArray();
  }

  filterData = (column, val) => {
    var filter = this.gridApi.getFilterInstance(column)
    var modelObj = null
    if (val !== "all") {
      modelObj = {
        type: "equals",
        filter: val
      }
    }
    filter.setModel(modelObj)
    this.gridApi.onFilterChanged()
  }

  filterSize = val => {
    if (this.gridApi) {
      this.gridApi.paginationSetPageSize(Number(val))
      this.setState({
        pageSize: val
      })
    }
  }
  toggleCollapse = () => {
    this.setState(state => ({collapse: !state.collapse}))
  }
  onEntered = () => {
    this.setState({status: "Opened"})
  }
  onEntering = () => {
    this.setState({status: "Opening..."})
  }

  onExiting = () => {
    this.setState({status: "Closing..."})
  }
  onExited = () => {
    this.setState({status: "Closed"})
  }

  render() {
    const {rowData, columnDefs, defaultColDef, pageSize} = this.state
    return (
      <Row className="app-quotations-list position-relative">
        <GetQuote open={this.state.getQuoteOpen}
                  quotationInfo={this.state.quotationInfo}
                  isOpenGetQuote={this.state.isOpenGetQuote}
                  setClose={this.handleClickClose}/>
        <Col sm="12">
          <Card
            className={classnames("card-action card-reload", {
              "d-none": this.state.isVisible === false,
              "card-collapsed": this.state.status === "Closed",
              closing: this.state.status === "Closing...",
              opening: this.state.status === "Opening...",
            }, 'box-shadow-0')}
          >
            <CardHeader>
              <div className='col-md-3 col-sm-12 col-lg-4 d-flex'>
                <CardTitle className='mr-3 font-large-1 text-nowrap'>
                  <Fab variant="extended" color="primary" aria-label="add" className='rounded box-shadow-1'
                       onClick={this.handleClickOpen}>
                    <Plus/> &nbsp;&nbsp;Get a quote
                  </Fab>
                </CardTitle>
              </div>
              <div className='bg-accent-1 col-md-9 col-sm-12 col-lg-8 d-flex justify-content-end'>
                <ButtonGroup className='box-shadow-0 rounded'>
                  {this.state.quotationType.map((elt, i) => {
                    return (
                      <Button
                        key={i}
                        className={classnames('rounded', 'bg-transparent', 'tabs-options', this.state.quoteIndex === i ? 'bottom-bord' : '')}
                        color='whitesmoke'
                        onClick={() => this.setState({quoteIndex: i})}>{elt}
                      </Button>
                    )
                  })}
                </ButtonGroup>
              </div>
              <div className='pl-3 mt-4'>
                <div>
                  Get and manage and manage all information related to the quotations hat you have done since the
                  registration. Accessibility available until the management of the quotation's files.
                </div>
              </div>
            </CardHeader>
          </Card>
        </Col>
        <Col sm="12">
          <Card>
            <CardBody>
              <div className="ag-theme-material ag-grid-table pb-5 pt-2" style={{height: "calc(100vh - 100px)"}}>
                <div className="ag-grid-actions d-flex justify-content-between flex-wrap mb-3">
                  <div className='d-flex align-items-center'>
                    <div className="sort-dropdown">
                      <UncontrolledDropdown className="ag-dropdown px-3 py-3 ml-3">
                        <DropdownToggle tag="div">
                          1 - {pageSize} of 150
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="div" onClick={() => this.filterSize(20)}>20</DropdownItem>
                          <DropdownItem tag="div" onClick={() => this.filterSize(50)}>50</DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(100)}
                          >
                            100
                          </DropdownItem>
                          <DropdownItem
                            tag="div"
                            onClick={() => this.filterSize(150)}
                          >
                            150
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledDropdown>
                    </div>
                  </div>
                  <div className="filter-actions col-5 d-flex align-items-center justify-content-end">
                    <IconButton className='box-shadow-1 round mr-3'>
                      <Search/>
                    </IconButton>
                    <IconButton className='box-shadow-1 round mr-3' onClick={this.loadDataArray}>
                      <Refresh/>
                    </IconButton>
                    <div className="dropdown actions-dropdown">
                      <UncontrolledButtonDropdown className='d-flex'>
                        <DropdownToggle className="px-2 py-75" color="white">
                          Actions
                          <ChevronDown className="ml-50" size={15}/>
                        </DropdownToggle>
                        <DropdownMenu right>
                          <DropdownItem tag="a">
                            <Trash2 size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Delete</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Clipboard size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Archive</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Printer size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">Print</span>
                          </DropdownItem>
                          <DropdownItem tag="a">
                            <Download size={15}/>&nbsp;&nbsp;
                            <span className="align-middle ml-50">CSV</span>
                          </DropdownItem>
                        </DropdownMenu>
                      </UncontrolledButtonDropdown>
                    </div>
                  </div>
                </div>
                <CustomBackdrop isOpen={this.state.isOpen}/>
                {this.state.rowData !== null ? (
                  <ContextLayout.Consumer>
                    {context => (
                      <AgGridReact
                        gridOptions={{roupUseEntireRow: true}}
                        rowSelection="multiple"
                        defaultColDef={defaultColDef}
                        columnDefs={columnDefs}
                        rowData={rowData}
                        colResizeDefault={"shift"}
                        animateRows={true}
                        floatingFilter={this.state.isFiltering}
                        pagination={true}
                        pivotPanelShow="always"
                        onSelectionChanged={this.onSelectionChanged}
                        paginationPageSize={pageSize}
                        resizable={true}
                        enableRtl={context.state.direction === "rtl"}
                      />
                    )}
                  </ContextLayout.Consumer>
                ) : null}
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}

export default Quotations
