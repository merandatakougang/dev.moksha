export default class UserModel {
  email
  password
  phoneNumber
  countryId
  firstName
  lastName
  city
  civility
  functionInCompany
  client


  constructor({email, password, phoneNumber, countryId, firstName, lastName, city, civility, functionInCompany, client}) {
    this.email = email;
    this.password = password;
    this.phoneNumber = phoneNumber;
    this.countryId = countryId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.city = city;
    this.civility = civility;
    this.functionInCompany = functionInCompany;
    this.client = client;
  }
}

class ClientModel {
  typeOfClient
  companyName
  companyAdress
  phoneNumber

  constructor({typeOfClient, companyName, companyAdress, phoneNumber}) {
    this.typeOfClient = typeOfClient;
    this.companyName = companyName;
    this.companyAdress = companyAdress;
    this.phoneNumber = phoneNumber;
  }
}
