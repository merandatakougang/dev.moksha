export default class QuotationsModel {


  cfrValue
  client
  commodityCustonCommodityName
  commodityDimensionsH
  commodityDimensionsL
  commodityDimensionsW
  commodityInOutCountry
  commodityOrigin
  commodityWeight
  containers
  createdAt
  id
  isAssurTrans
  isCollection
  isEtcn
  isLocalInsurance
  isPhytosanitaryInspection
  isPositioningContainer
  isSeaInsurance
  isStuffing
  isTransport
  isTva
  isUnloadingContainer
  isWoodenCase
  operatingsPlace
  packaging
  quotationValue
  reference
  status
  transactionType
  transportMode
  updatedAt
  user
  userClientId
  userCreatorId


  constructor({cfrValue, client, commodityCustonCommodityName, commodityDimensionsH, commodityDimensionsL, commodityDimensionsW, commodityInOutCountry, commodityOrigin, commodityWeight, containers, createdAt, id, isAssurTrans, isCollection, isEtcn, isLocalInsurance, isPhytosanitaryInspection, isPositioningContainer, isSeaInsurance, isStuffing, isTransport, isTva, isUnloadingContainer, isWoodenCase, operatingsPlace, packaging, quotationValue, reference, status, transactionType, transportMode, updatedAt, user, userClientId, userCreatorId}) {
    this.cfrValue = cfrValue;
    this.client = client;
    this.commodityCustonCommodityName = commodityCustonCommodityName;
    this.commodityDimensionsH = commodityDimensionsH;
    this.commodityDimensionsL = commodityDimensionsL;
    this.commodityDimensionsW = commodityDimensionsW;
    this.commodityInOutCountry = commodityInOutCountry;
    this.commodityOrigin = commodityOrigin;
    this.commodityWeight = commodityWeight;
    this.containers = containers;
    this.createdAt = createdAt;
    this.id = id;
    this.isAssurTrans = isAssurTrans;
    this.isCollection = isCollection;
    this.isEtcn = isEtcn;
    this.isLocalInsurance = isLocalInsurance;
    this.isPhytosanitaryInspection = isPhytosanitaryInspection;
    this.isPositioningContainer = isPositioningContainer;
    this.isSeaInsurance = isSeaInsurance;
    this.isStuffing = isStuffing;
    this.isTransport = isTransport;
    this.isTva = isTva;
    this.isUnloadingContainer = isUnloadingContainer;
    this.isWoodenCase = isWoodenCase;
    this.operatingsPlace = operatingsPlace;
    this.packaging = packaging;
    this.quotationValue = quotationValue;
    this.reference = reference;
    this.status = status;
    this.transactionType = transactionType;
    this.transportMode = transportMode;
    this.updatedAt = updatedAt;
    this.user = user;
    this.userClientId = userClientId;
    this.userCreatorId = userCreatorId;
  }

  toDatabase() {
    return {
      ...this
    }
  }

  toDataArray() {
    return {
      id: this.id,
      num: this.reference,
      date: this.createdAt,
      service: this.transactionType?.name + ' ' + this.transportMode?.name,
      packaging: this.packaging?.name,
      amount: this.quotationValue,
      status: this.status,
    }
  }
}
