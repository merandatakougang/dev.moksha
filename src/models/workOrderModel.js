export default class WorkOrderModel {
  constructor({id, reference, quotation, blNumber, createdAt}) {
    this.id = id
    this.reference = reference
    this.quotation = quotation
    this.blNumber = blNumber
    this.data = createdAt
  }

  toDatabase() {
    return {
      ...this
    }
  }

  toDataArray() {
    return {
      id: this.id,
      reference: this.reference,
      blNumber: this.blNumber,
      quotation: this.quotation?.reference,
      num_quotation: this.quotation?.id,
      service: this.quotation?.transactionType?.name + ' ' + this.quotation?.transportMode?.name,
      packaging: this.packaging?.name,
      amount: this.quotationValue,
      status: this.quotation.status,
      date: this.date,
    }
  }
}
