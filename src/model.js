// cfrValue: 8080000
// client: {}
// commodityCustonCommodityName: "Fournitures diverses"
// commodityDimensionsH: 250
// commodityDimensionsL: 250
// commodityDimensionsW: 250
// commodityInOutCountry: {id: 40, createdAt: "2020-09-04T09:35:30.209Z", updatedAt: "2020-09-04T09:35:30.209Z", name: "Cameroon", code: 237, …}
// commodityOrigin: {id: 1, createdAt: "2020-09-08T09:22:32.255Z", updatedAt: "2020-09-08T09:22:32.255Z", name: "Africa & Europe"}
// commodityWeight: 404
// containers: []
// id: 1
// isAssurTrans: false
// isCollection: false
// isEtcn: true
// isLocalInsurance: true
// isPhytosanitaryInspection: null
// isPositioningContainer: null
// isSeaInsurance: false
// isStuffing: null
// isTransport: false
// isTva: true
// isUnloadingContainer: null
// isWoodenCase: false
// operatingsPlace: {id: 1, createdAt: "2020-09-04T09:55:47.834Z", updatedAt: "2020-09-04T09:55:47.834Z", name: "Douala"}
// updatedAt: "2020-09-08T09:23:08.383Z"
// user: {}
// userClientId: 1
// userCreatorId: 1
