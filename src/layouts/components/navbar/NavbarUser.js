import React from "react"
import {
  NavItem,
  NavLink,
  UncontrolledDropdown,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap"
import axios from "axios"
import * as Icon from "react-feather"
import classnames from "classnames"
import ReactCountryFlag from "react-country-flag"
import Autocomplete from "../../../components/@vuexy/autoComplete/AutoCompleteComponent"
import {useAuth0} from "../../../authServices/auth0/auth0Service"
import {history} from "../../../history"
import {IntlContext} from "../../../utility/context/Internationalization"
import {faExpand, faFile} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faQuestionCircle} from "@fortawesome/free-regular-svg-icons";

const handleNavigation = (e, path) => {
  e.preventDefault()
  history.push(path)
}

const UserDropdown = props => {
  const {logout, isAuthenticated} = useAuth0()
  return (
    <DropdownMenu right>
      <DropdownItem
        onClick={e => handleNavigation(e, "/pages/profile")}
      >
        <Icon.User size={14} className="mr-3 my-1"/>
        <span className="align-middle">Edit Profile</span>
      </DropdownItem>
      <DropdownItem
        tag="a"
        href="#"
        onClick={e => handleNavigation(e, "/email/inbox")}
      >
        <Icon.Mail size={14} className="mr-3 my-1"/>
        <span className="align-middle">My Inbox</span>
      </DropdownItem>
      <DropdownItem
        onClick={e => handleNavigation(e, "/todo/all")}
      >
        <Icon.CheckSquare size={14} className="mr-3 my-1"/>
        <span className="align-middle">Tasks</span>
      </DropdownItem>
      <DropdownItem
        onClick={e => handleNavigation(e, "/chat")}
      >
        <Icon.MessageSquare size={14} className="mr-3 my-1"/>
        <span className="align-middle">Chats</span>
      </DropdownItem>
      <DropdownItem onClick={e => handleNavigation(e, "/ecommerce/wishlist")}>
        <Icon.Heart size={14} className="mr-3 my-1"/>
        <span className="align-middle">WishList</span>
      </DropdownItem>
      <DropdownItem divider/>
      <DropdownItem
        onClick={e => {
          e.preventDefault()
          if (isAuthenticated) {
            return logout({
              returnTo: window.location.origin + process.env.REACT_APP_PUBLIC_PATH
            })
          } else {
            const provider = props.loggedInWith
            if (provider !== null) {
              if (provider === "jwt") {
                return props.logoutWithJWT()
              }
              if (provider === "firebase") {
                return props.logoutWithFirebase()
              }
            } else {
              history.push("/login")
            }
          }

        }}
      >
        <Icon.Power size={14} className="mr-3 my-1"/>
        <span className="align-middle">Log Out</span>
      </DropdownItem>
    </DropdownMenu>
  )
}

class NavbarUser extends React.PureComponent {
  state = {
    navbarSearch: false,
    langDropdown: false,
    shoppingCart: [],
    suggestions: []
  }

  componentDidMount() {
    axios.get("/api/main-search/data").then(({data}) => {
      this.setState({suggestions: data.searchResult})
    })
  }

  handleNavbarSearch = () => {
    this.setState({
      navbarSearch: !this.state.navbarSearch
    })
  }

  removeItem = id => {
    let cart = this.state.shoppingCart

    let updatedCart = cart.filter(i => i.id !== id)

    this.setState({
      shoppingCart: updatedCart
    })
  }

  handleLangDropdown = () =>
    this.setState({langDropdown: !this.state.langDropdown})

  render() {
    const renderCartItems = this.state.shoppingCart.map(item => {
      return (
        <div className="cart-item" key={item.id}>

        </div>
      )
    })

    return (
      <ul className="nav navbar-nav navbar-nav-user float-right d-flex align-items-center">
        <NavItem className="nav-item">
          <NavLink className="nav-link-search">
            <Icon.Home size={24}/>
          </NavLink>
        </NavItem>

        <NavItem className="nav-search" onClick={this.handleNavbarSearch}>
          <NavLink className="nav-link-search">
            <Icon.Search size={24} data-tour="search"/>
          </NavLink>
          <div
            className={classnames("search-input", {
              open: this.state.navbarSearch,
              "d-none": this.state.navbarSearch === false
            })}
          >
            <div className="search-input-icon">
              <Icon.Search size={20} className="primary"/>
            </div>
            <Autocomplete
              className="form-control"
              suggestions={this.state.suggestions}
              filterKey="title"
              filterHeaderKey="groupTitle"
              grouped={true}
              placeholder="Explore Vuexy..."
              autoFocus={true}
              clearInput={this.state.navbarSearch}
              externalClick={() => {
                this.setState({navbarSearch: false})
              }}
              onKeyDown={e => {
                if (e.keyCode === 27 || e.keyCode === 13) {
                  this.setState({
                    navbarSearch: false
                  })
                  this.props.handleAppOverlay("")
                }
              }}
              customRender={(
                item,
                i,
                filteredData,
                activeSuggestion,
                onSuggestionItemClick,
                onSuggestionItemHover
              ) => {
                const IconTag = Icon[item.icon ? item.icon : "X"]
                return (
                  <li
                    className={classnames("suggestion-item", {
                      active: filteredData.indexOf(item) === activeSuggestion
                    })}
                    key={i}
                    onClick={e => onSuggestionItemClick(item.link, e)}
                    onMouseEnter={() =>
                      onSuggestionItemHover(filteredData.indexOf(item))
                    }
                  >
                    <div
                      className={classnames({
                        "d-flex justify-content-between align-items-center":
                          item.file || item.img
                      })}
                    >
                      <div className="item-container d-flex">
                        {item.icon ? (
                          <IconTag size={20}/>
                        ) : item.file ? (
                          <img
                            src={item.file}
                            height="36"
                            width="36"
                            alt={item.title}
                          />
                        ) : item.img ? (
                          <img
                            className="rounded-circle mt-25"
                            src={item.img}
                            height="36"
                            width="36"
                            alt={item.title}
                          />
                        ) : null}
                        <div className="item-info ml-1">
                          <p className="align-middle mb-0">{item.title}</p>
                          {item.by || item.email ? (
                            <small className="text-muted">
                              {item.by
                                ? item.by
                                : item.email
                                  ? item.email
                                  : null}
                            </small>
                          ) : null}
                        </div>
                      </div>
                      {item.size || item.date ? (
                        <div className="meta-container">
                          <small className="text-muted">
                            {item.size
                              ? item.size
                              : item.date
                                ? item.date
                                : null}
                          </small>
                        </div>
                      ) : null}
                    </div>
                  </li>
                )
              }}
              onSuggestionsShown={userInput => {
                if (this.state.navbarSearch) {
                  this.props.handleAppOverlay(userInput)
                }
              }}
            />
            <div className="search-input-close">
              <Icon.X
                size={20}
                onClick={(e) => {
                  e.stopPropagation()
                  this.setState({
                    navbarSearch: false
                  })
                  this.props.handleAppOverlay("")
                }}
              />
            </div>
          </div>

        </NavItem>
        <NavItem className="nav-item">
          <NavLink className="nav-link-search">
            <FontAwesomeIcon icon={faExpand} size='lg'/>
          </NavLink>
        </NavItem>
        <NavItem className="nav-item">
          <NavLink className="nav-link-search">
            <FontAwesomeIcon icon={faQuestionCircle} size='lg'/>
          </NavLink>
        </NavItem>
        <IntlContext.Consumer >
          {context => {
            let langArr = {
              "en": "EN",
              "fr": "FR",
            }
            return (
              <Dropdown
                tag="li"
                className="dropdown-language nav-item mt-1 align-items-center"
                isOpen={this.state.langDropdown}
                toggle={this.handleLangDropdown}
                data-tour="language">
                <DropdownToggle
                  tag="a"
                  className="nav-link">
                  <ReactCountryFlag
                    className="country-flag"
                    countryCode={context.state.locale === "en" ? "US" : context.state.locale}
                    svg/>
                  <span className="d-sm-inline-block d-none text-capitalize align-middle ml-50">
                    {langArr[context.state.locale]}
                  </span>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem tag="a" onClick={() => context.switchLanguage("fr")}>
                    <ReactCountryFlag className="country-flag" countryCode="fr" svg/>
                    <span className="ml-1">FR</span>
                  </DropdownItem>
                  <DropdownItem tag="a" onClick={() => context.switchLanguage("en")}>
                    <ReactCountryFlag className="country-flag rounded" countryCode="us" svg/>
                    <span className="ml-1">EN</span>
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            )
          }}
        </IntlContext.Consumer>
        <NavItem className="nav-item align-nav-profile mt-1">
          <NavLink className="nav-link-search">
            <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
              <DropdownToggle tag="a" className="nav-link dropdown-user-link">
                <span data-tour="user">
                  <img
                    src={this.props.userImg}
                    className="round"
                    height="32"
                    width="32"
                    alt="avatar"
                  />
                </span>
              </DropdownToggle>
              <UserDropdown {...this.props} />
            </UncontrolledDropdown>
          </NavLink>
        </NavItem>
      </ul>
    )
  }
}

export default NavbarUser
