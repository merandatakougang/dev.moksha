import DataService from "./dataService";

export default class AuthService extends DataService {

  login = (data) => {
    return this.post('/auth/login', data)
  }

  register = (data) => {
    return this.post('/auth/register', data)
  }

  changePassword = (data) => {
    return this.post('/auth/changePassword', data)
  }

  sendResetPasswordEmail = (data) => {
    return this.post('/auth/sendResetPasswordEmail', data)
  }

  forgottenPassword = (data, config) => {
    return this.post('/auth/forgottenPassword', data, config)
  }

  getProfile = (data, config) => {
    return this.get(`/auth/user/${data}`, config)
  }
}
