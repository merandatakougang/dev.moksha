import axios from "axios";

export default class DataService {

  constructor() {
    this.client = axios.create({
      baseURL: 'https://api.itrade.digital',
      headers: {
        'content-type': 'application/json',
        // 'x-rapidapi-host': 'example.com',
        // 'x-rapidapi-key': process.env.RAPIDAPI_KEY
      }
    })
  }

  post = (url, data, config) => {
    return this.client.post(url, data, config)
  }

  get = (url, config) => {
    return this.client.get(url, config)
  }


}
