import DataService from "./dataService";

export default class QuotationService extends DataService {

  getQuotationsByUser = (id) => {
    return this.get('/quotations/user/1')
  }

  getQuotationInfo = () => {
    return this.get('/quotations/info')
  }
}
